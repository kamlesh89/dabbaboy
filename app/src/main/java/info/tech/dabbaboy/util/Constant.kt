package info.tech.dabbaboy.util

class Constant {

    companion object {

        val SPLASH_TIMEOUT: Long = 3000

        const val OTPACTIVITY_FORGOT = "forgot"
        const val OTPACTIVITY_LOGIN = "login"

        // address Intent
        var ADDRESS_INTENT_CODE = 123

        // vendor package list menu type
        var ITEM_TYPE_LUNCH = "Lunch"

        // schedule meal text
        val REMOVE_LUNCH = "Remove Lunch"
        val REMOVE_DINNER = "Remove Dinner"
        val ADD_LUNCH = "Add Lunch"
        val ADD_DINNER = "Add Dinner"

        val lunch_time_limit = 9
        val dinner_time_limit = 17

        val INTENT_PAYMENT = 12345
    }
}