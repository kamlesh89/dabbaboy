package info.tech.dabbaboy.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.core.app.ActivityCompat;

import info.tech.dabbaboy.activity.onborad.LoginActivity;
import info.tech.dabbaboy.activity.MainActivity;


public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context ctx;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "sessionmanager";
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL_ID = "email";
    public static final String KEY_MOBILE = "mobile";

    public static final String KEY_AREA = "area";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_ADDRESS_ID = "address_id";
    public static final String KEY_ADDRESS_DETAILS = "address_details";

    public static final String KEY_USER_SUBSCRIBE = "user_subscribe";
    public static final String KEY_USER_AMOUNT = "user_amount";
    public static final String KEY_USER_TIFFIN = "user_tiffin";
    public static final String KEY_USER_LUNCH_ADD = "user_lunch_address";
    public static final String KEY_USER_DINNER_ADD = "user_dinner_address";
    public static final String KEY_USER_PACKAGE_ID = "pack_id";

    // selected order id
    public static final String KEY_SELECTED_ORDER_ID = "selected order id";
    // select item
    public static final String KEY_USER_SELECTED_ITEM = "item_name";
    public static final String KEY_USER_SELECTED_ITEM_ID = "item_id";
    // selected time
    public static final String KEY_USER_SELECTED_TIME = "selected_time";
    // selected position for schedule
    public static final String KEY_SCHEDULE_POSISTION = "schedule_position";
    // selected position for buy extra
    public static final String KEY_EXTRA_POSISTION = "extra_position";
    // selected position for menu
    public static final String KEY_MENU_POSISTION = "menu_position";

    public SessionManager(Context context) {
        this.ctx = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String id, String name, String email, String mobile, String area, String pin) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL_ID, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_AREA, area);
        editor.putString(KEY_PINCODE, pin);
        editor.commit();
    }

    public void setData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void setIntData(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(ctx, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(i);
        }
    }

    public void logoutUser(MainActivity mainActivity) {
        editor.clear();
        editor.commit();
        ActivityCompat.finishAffinity(mainActivity);
        Intent i = new Intent(ctx.getApplicationContext(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public String getData(String key) {
        return pref.getString(key, null);
    }

    public int getIntData(String key) {
        return pref.getInt(key, 0);
    }
}
