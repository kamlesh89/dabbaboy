package info.tech.dabbaboy.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.util.DisplayMetrics
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import info.tech.dabbaboy.BuildConfig
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.activity.subscription.BuyExtraActivity
import info.tech.dabbaboy.activity.subscription.SchedulingActivity
import info.tech.dabbaboy.activity.subscription.TodaysMenuActivity
import info.tech.dabbaboy.pojo.OrderDates
import org.json.JSONObject
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class Utility {
    companion object {

        lateinit var dialog: Dialog

        @JvmStatic
        fun showLoader(ctx: Context) {
            dialog = Dialog(ctx)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.setContentView(R.layout.layout_progress)
            dialog.setCancelable(false)
            dialog.show()
        }

        @JvmStatic
        fun hideLoader() {
            if (dialog.isShowing) {
                dialog.dismiss()
            }
        }

        fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

/*
        fun setFragment(activity: Activity, fragment: Fragment) {
            (activity as AppCompatActivity).supportFragmentManager
                .beginTransaction()
                .replace(
                    R.id.fragment_container,
                    fragment
                ).commit()
        }
*/

        fun getDate(): String {
            val c = Calendar.getInstance().time
            val df = SimpleDateFormat("yyyy-MM-dd")
            return df.format(c)
        }

        fun setFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as MainActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, tag)
                .commit()
        }

        fun setScheduleFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as SchedulingActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_schdule_fragment, fragment, tag)
                .commit()
        }

        fun setExtraFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as BuyExtraActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_extra_fragment, fragment, tag)
                .commit()
        }

        fun setMenuFragment(fragment: Fragment, tag: String, activity: Activity) {
            (activity as TodaysMenuActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_menu_fragment, fragment, tag)
                .commit()
        }


        @JvmStatic
        fun toastView(msg: String, ctx: Context) {
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
        }

        fun referFriend(ctx: Context, code: String) {

            var content =
                "Install DabbaBoy application using my refer code : " + code + "\n\nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, content);
            ctx.startActivity(Intent.createChooser(shareIntent, "Share via"))
        }

        @JvmStatic
        fun snacbarShow(msg: String, coordinatorLayout: CoordinatorLayout) {
            val snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG).show()
        }

        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view: View? = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        }

        fun getWidth(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager =
                context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        fun getHight(context: Context): Int {
            val displayMetrics = DisplayMetrics()
            val windowmanager =
                context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.heightPixels
        }

        fun getMonths(dateList: ArrayList<OrderDates>): ArrayList<String> {
            var months: ArrayList<String> = ArrayList<String>()
            for (i in 0..dateList.size - 1) {
                var month = dateList[i].date.substring(5, 7)
                var month_name = MyCalendar.getMonthName(month);
                if (!months.contains(month_name)) {
                    months.add(month_name)
                }
            }
            return months
        }

        fun getYear(dateList: java.util.ArrayList<OrderDates>): ArrayList<String> {
            var years: ArrayList<String> = ArrayList<String>()
            for (i in 0..dateList.size - 1) {
                var year = dateList[i].date.substring(0, 4)
                if (!years.contains(year)) {
                    years.add(year)
                }
            }
            return years
        }

        fun getnextDay(date: String): String {

            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val d = sdf.parse(date)

            val cal = Calendar.getInstance()
            cal.time = d
            cal.add(Calendar.DAY_OF_MONTH, 1) //Adds a day
            var newdate = sdf.format(cal.time)
            return newdate
        }

        // get address from object
        fun getAddress(lunchAddress: JSONObject?): String {
            var builder = StringBuilder()
            builder.append(lunchAddress?.getString("building_name") + " ")
            builder.append(lunchAddress?.getString("street") + " ")
            builder.append(lunchAddress?.getString("landmark") + " ")
            builder.append(lunchAddress?.getString("area"))
            return builder.toString()
        }

        fun changeDateFormat(date: String): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val d: Date = sdf.parse(date)

            val new_sdf = SimpleDateFormat("dd-MMM-yyyy")
            val new_date = new_sdf.format(d)
            return new_date.toString()
        }

    }
}