package info.tech.dabbaboy.util

import info.tech.dabbaboy.pojo.OrderDates
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList

class ApiObjects {
    companion object {

        // add addresss
        @JvmStatic
        fun getSubmitOrder(
            user_id: String,
            package_id: String,
            qty: String,
            startdate: String,
            lunch: String,
            dinner: String,
            lunch_time: String,
            dinnertime: String,
            lunch_address_id: String?,
            dinner_address_id: String?,
            totalamont: String,
            pay_method: String,
            pack_name: String,
            tiffin_type: String,
            charges: String,
            discount: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("user_id", user_id)
            myobject.put("package_id", package_id)
            myobject.put("startdate", startdate)
            myobject.put("lunch", lunch)
            myobject.put("dinner", dinner)
            myobject.put("quantity", qty)
            myobject.put("lunch_time", lunch_time)
            myobject.put("dinner_time", dinnertime)
            myobject.put("lunch_address_id", lunch_address_id)
            myobject.put("dinner_address_id", dinner_address_id)
            myobject.put("TotalAmount", totalamont)
            myobject.put("payment_method", pay_method)
            myobject.put("tiffin_type", tiffin_type)
            myobject.put("ChargesAmount", charges)
            myobject.put("type_amount", charges)
            myobject.put("discount_amount", discount)
            myobject.put(
                "data_array", DateOperation.getSubmitAllDate(
                    lunch, dinner, startdate
                    , lunch_address_id, dinner_address_id, pack_name, qty,
                    package_id, tiffin_type
                )
            )
            return myobject
        }

        // submit free order
        @JvmStatic
        fun getSubmitfreeOrder(
            user_id: String, date: String,
            time: String, type: String,
            address_id: String?
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("user_id", user_id)
            myobject.put("delivery_time", time)
            myobject.put("service_type", type)
            myobject.put("address_id", address_id)
            myobject.put("order_date", date)
            return myobject
        }

        // add addresss
        fun getInsertAddress(
            address_type: String, building: String, street: String,
            landmark: String, pincode: String, area: String,
            city: String, state: String, user_id: String,
            lati: String, longi: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("address_type", address_type)
            myobject.put("building_name", building)
            myobject.put("street", street)
            myobject.put("landmark", landmark)
            myobject.put("pin_code", pincode)
            myobject.put("area", area)
            myobject.put("city", city)
            myobject.put("state", state)
            myobject.put("user_id", user_id)
            myobject.put("latitude", lati)
            myobject.put("longitude", longi)
            return myobject
        }

        // edit addresss
        fun updateAddress(
            address_type: String,
            building: String,
            street: String,
            landmark: String,
            pincode: String,
            area: String,
            city: String,
            state: String,
            user_id: String,
            address_id: String,
            latitude: String,
            longitude: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("address_type", address_type)
            myobject.put("building_name", building)
            myobject.put("street", street)
            myobject.put("landmark", landmark)
            myobject.put("pin_code", pincode)
            myobject.put("area", area)
            myobject.put("city", city)
            myobject.put("state", state)
            myobject.put("user_id", user_id)
            myobject.put("address_id", address_id)
            myobject.put("latitude", latitude)
            myobject.put("longitude", longitude)
            return myobject
        }

        // get user details
        fun getUserdetails(user_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("user_id", user_id)
            return myobject
        }

        // delete addresss
        @JvmStatic
        fun getDeleteAddress(address_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "2")
            myobject.put("address_id", address_id)
            return myobject
        }

        //  addresss list
        @JvmStatic
        fun getAlladdress(user_id: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("user_id", user_id)
            return myobject
        }

        // only mode
        @JvmStatic
        fun getMode(mode: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", mode)
            return myobject
        }

        // get Area
        fun getArea(pincode: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("pin_code", pincode)
            return myobject
        }

        // forgot password
        fun getforgotRequest(mobile: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("mobile_no", mobile)
            return myobject
        }

        fun getforgotVerifyOTP(mobile: String, otp: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("mobile_no", mobile)
            myobject.put("otp", otp)
            return myobject
        }

        fun getforgotcreatePass(mobile: String, pass: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "2")
            myobject.put("mobile_no", mobile)
            myobject.put("new_password", pass)
            return myobject
        }


        // login registration

        fun getUserRegister(
            name: String, mobile: String, email: String,
            address: String, city: String, state: String, pincode: String, pass: String
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "2")
            myobject.put("name", name)
            myobject.put("mobile_no", mobile)
            myobject.put("email", email)
            myobject.put("city", city)
            myobject.put("state", state)
            myobject.put("address", address)
            myobject.put("pincode", pincode)
            myobject.put("password", pass)
            return myobject
        }

        fun getVerifyOTP(mobile: String, otp: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "1")
            myobject.put("mobile_no", mobile)
            myobject.put("otp", otp)
            return myobject
        }


        fun getUserLogin(mobile: String, pass: String): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("mobile_no", mobile)
            myobject.put("password", pass)
            return myobject
        }

        fun getUserData(user_id: String?): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("user_id", user_id)
            return myobject
        }

        fun getvendorCombo(
            pincode: String,
            type: String,
            todayOrder: OrderDates
        ): JSONObject {
            var myobject = JSONObject()
            myobject.put("mode", "0")
            myobject.put("pin_code", pincode)
            myobject.put("package_id", todayOrder.pack_id)
            myobject.put("service_type", type)
            myobject.put("item_date", todayOrder.date)
            myobject.put("order_id", todayOrder.order_id)
            return myobject
        }

        fun getSelectCombo(
            userid: String?,
            vendorId: String,
            order_id: String,
            type: String?,
            date: String?,
            pack_id: String?,
            idList: ArrayList<String>,
            mode: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", mode)
            obj.put("user_id", userid)
            obj.put("order_id", order_id)
            obj.put("vendor_id", vendorId)
            obj.put("service_type", type)
            obj.put("order_date", date)
            obj.put("package_id", pack_id)
            var array = JSONArray()
            for (i in 0..idList.size - 1) {
                var id_obj = JSONObject()
                id_obj.put("item_id", idList[i])
                array.put(id_obj)
            }
            obj.put("item_id", array)
            return obj
        }

        fun getOrderdates(orderid: String?): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("order_id", orderid)
            return obj
        }

        fun updatedTime(orderid: String?, type: String, selectedTime: String?): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("order_id", orderid)
            obj.put("service_type", type)
            obj.put("time", selectedTime)
            return obj
        }

        fun updateAddressid(
            orderid: String?,
            type: String,
            address_id: String?,
            update_address_id: String?
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("order_id", orderid)
            obj.put("service_type", type)
            obj.put("address_id", address_id)
            obj.put("update_address_id", update_address_id)
            return obj
        }

        fun getupdateDailyOrder(
            order: OrderDates,
            lastorder: OrderDates,
            lunch_add_id: String,
            dinner_add_id: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")

            // insert
            var insert_arr = JSONArray()
            var insert_obj = JSONObject()
            insert_obj.put("order_id", order.order_id)
            insert_obj.put("date", Utility.getnextDay(lastorder.date))
            insert_obj.put("lunch_qty", order.lunch_qty)
            insert_obj.put("dinner_qty", order.dinner_qty)
            insert_obj.put("lunch_vendor_id", order.lunch_vendor_id)
            insert_obj.put("dinner_vendor_id", order.dinner_vendor_id)
            insert_obj.put("lunch_status", order.lunch_status)
            insert_obj.put("dinner_status", order.dinner_status)
            insert_obj.put("lunch_address_id", lunch_add_id)
            insert_obj.put("dinner_address_id", dinner_add_id)
            insert_obj.put("pack_name", order.package_name)
            insert_arr.put(insert_obj)

            var update_arr = JSONArray()
            var update_obj = JSONObject()
            update_obj.put("id", order.id)
            update_obj.put("lunch_qty", "0")
            update_obj.put("dinner_qty", "0")
            update_obj.put("lunch_vendor_id", "0")
            update_obj.put("dinner_vendor_id", "0")
            update_obj.put("lunch_status", "0")
            update_obj.put("dinner_status", "0")
            update_obj.put("lunch_address_id", "0")
            update_obj.put("dinner_address_id", "0")
            update_arr.put(update_obj)

            obj.put("insert_order", insert_arr)
            obj.put("update_order", update_arr)
            return obj
        }

        fun getUpdateAddress(type: String, addressId: String, order_id: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("order_id", order_id)
            obj.put("service_type", type)
            obj.put("address_id", addressId)
            return obj
        }

        // for order hostory
        fun getOrderHistor(userid: String?, order_id: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("user_id", userid)
            obj.put("order_id", order_id)
            return obj
        }

        // for user profile
        fun getUserProfile(user_id: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("user_id", user_id)
            return obj
        }

        // for paytm checksum
        @JvmStatic
        fun generatePaytmChecksum(
            mid: String,
            order_id: String,
            user_id: String,
            industry_id: String,
            channel_id: String,
            amount: String,
            website: String,
            callback_url: String,
            email: String,
            mobile: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("MID", mid)
            obj.put("ORDER_ID", order_id)
            obj.put("USER_ID", user_id)
            obj.put("INDUSTRY_TYPE_ID", industry_id)
            obj.put("CHANNEL_ID", channel_id)
            obj.put("TXN_AMOUNT", amount)
            obj.put("WEBSITE", website)
            obj.put("CALLBACK_URL", callback_url)
            obj.put("EMAIL", email)
            obj.put("MOBILE_NO", mobile)
            return obj
        }

        // for paytm checksum
        fun updateStartdate(order_id: String, date: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("order_id", order_id)
            obj.put("order_date", date)
            return obj
        }

    }
}