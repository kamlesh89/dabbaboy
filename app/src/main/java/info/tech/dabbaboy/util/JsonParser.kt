package info.tech.dabbaboy.util

import info.tech.dabbaboy.pojo.*
import okhttp3.internal.http2.Header
import org.json.JSONArray
import org.json.JSONObject
import java.lang.StringBuilder

class JsonParser {

    companion object {

        // time list for spinner
        fun gettimelist(array: JSONArray): ArrayList<String> {
            var list = ArrayList<String>()
            list.add("Select Time")
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var time1 = obj.getString("time_1")
                    var time2 = obj.getString("time_2")
                    list.add(time1 + " To " + time2)
                }
            }
            return list
        }

        // time list for dialog
        fun getupdatetimelist(array: JSONArray): ArrayList<String> {
            var list = ArrayList<String>()
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var time1 = obj.getString("time_1")
                    var time2 = obj.getString("time_2")
                    list.add(time1 + " To " + time2)
                }
            }
            return list
        }

        // varient list
        fun getVarientlist(array: JSONArray): ArrayList<Varient> {
            var list = ArrayList<Varient>()
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var id = obj.getString("id")
                    var days = obj.getString("day")
                    var price = obj.getString("price")
                    list.add(Varient(id, days, price))
                }
            }
            return list
        }

        // combo item details
        @JvmStatic
        fun getCombodetails(list: List<VendorPackage>): String {
            var build = StringBuilder()
            if (list.size > 0) {
                for (i in 0..list.size - 1) {
                    var type = list[i].service_type
                    if (type.equals(Constant.ITEM_TYPE_LUNCH)) {
                        build.append(list[i].item_name)
                        if (i < list.size - 1) {
                            build.append(", ")
                        }
                    }
                }
            }
            return build.toString()
        }

        // address list
        public fun getAddressList(array: JSONArray): ArrayList<Address> {
            var list = ArrayList<Address>()
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var id = obj.getString("id")
                    var type = obj.getString("address_type")
                    var building = obj.getString("building_name")
                    var street = obj.getString("street")
                    var landmark = obj.getString("landmark")
                    var city = obj.getString("city")
                    var state = obj.getString("state")
                    var pincode = obj.getString("pin_code")
                    var area = obj.getString("area")
                    var lati = obj.getDouble("latitude")
                    var longi = obj.getDouble("longitude")
                    list.add(
                        Address(
                            id,
                            type,
                            building,
                            street,
                            landmark,
                            pincode,
                            area,
                            city,
                            lati,
                            longi
                        )
                    )
                }
            }
            return list
        }

        fun getAreaList(array: JSONArray): ArrayList<String> {
            var list = ArrayList<String>()
            list.add("Select Area")
            if (array.length() > 0) {
                for (i in 0..array.length() - 1) {
                    var obj = array.getJSONObject(i)
                    var name = obj.getString("location_name")
                    list.add(name)
                }
            }
            return list
        }

        @JvmStatic
        fun getPackagelist(dataarray: JSONArray): MutableList<MyMeal> {
            var list = ArrayList<MyMeal>()
            if (dataarray.length() > 0) {
                for (i in 0..dataarray.length() - 1) {
                    var data_obj = dataarray.getJSONObject(i)
                    list.add(
                        MyMeal(
                            data_obj.getString("id"),
                            data_obj.getString("menu_name"),
                            data_obj.getString("package_image"),
                            data_obj.getJSONArray("variant"),
                            data_obj.getJSONArray("vendorpack")
                        )
                    )
                }
            }
            return list
        }

        @JvmStatic
        fun getvendorPackagelist(mymeal: MyMeal): MutableList<VendorPackage> {
            var list = ArrayList<VendorPackage>()
            if (mymeal.package_array.length() > 0) {
                for (i in 0..mymeal.package_array.length() - 1) {
                    var data_obj = mymeal.package_array.getJSONObject(i)
                    var service_type = data_obj.getString("service_type")
                    var item_name = data_obj.getString("item_name")
                    list.add(
                        VendorPackage(
                            mymeal.id,
                            mymeal.mealname,
                            mymeal.image,
                            service_type,
                            item_name,
                            mymeal.varient_array.toString()
                        )
                    )
                }
            }
            return list
        }

        fun getFixitems(fixArray: JSONArray): String {
            var builder = StringBuilder()
            if (fixArray.length() > 0) {
                for (x in 0..fixArray.length() - 1) {
                    var data_obj = fixArray.getJSONObject(x)
                    builder.append(data_obj.getString("item_name"))
                    if (x < fixArray.length() - 1) {
                        builder.append(", ")
                    }
                }
            }
            return builder.toString()
        }

        fun getvendorCombo(fixitem: String, vendorArray: JSONArray): ArrayList<VendorCombo> {
            var list = ArrayList<VendorCombo>()
            if (vendorArray.length() > 0) {
                for (x in 0..vendorArray.length() - 1) {
                    var data_obj = vendorArray.getJSONObject(x)
                    var name = data_obj.getString("name")
                    var id = data_obj.getString("id")
                    var menu = data_obj.getJSONArray("Menu")
                    list.add(VendorCombo(fixitem, name, id, menu))
                }
            }
            return list
        }

        fun getHeaderMenu(menu: JSONArray): ArrayList<HeaderMenu> {
            var list = ArrayList<HeaderMenu>()
            if (menu.length() > 0) {
                for (x in 0..menu.length() - 1) {
                    var data_obj = menu.getJSONObject(x)
                    var type = data_obj.getString("service_type")
                    var itemname = data_obj.getString("item_name")
                    var header_id = data_obj.getString("header_id")
                    var item = data_obj.getJSONArray("Item")
                    list.add(HeaderMenu(type, itemname, header_id, "", item))
                }
            }
            return list
        }

        fun getItemList(items: JSONArray): ArrayList<Items> {
            var list = ArrayList<Items>()
            if (items.length() > 0) {
                for (x in 0..items.length() - 1) {
                    var data_obj = items.getJSONObject(x)
                    var itemname = data_obj.getString("item_name")
                    var id = data_obj.getString("id")
                    list.add(Items(id, itemname))
                }
            }
            return list
        }

        // get active all package of user
        fun getActivePackage(dataArray: JSONArray): ArrayList<ActivePackage> {
            var list = ArrayList<ActivePackage>()
            if (dataArray.length() > 0) {
                for (x in 0..dataArray.length() - 1) {
                    var lunch_obj: JSONObject? = null
                    var dinner_obj: JSONObject? = null
                    var data_obj = dataArray.getJSONObject(x)
                    var id = data_obj.getString("id")
                    var lunch_id = data_obj.getString("lunch_address_id")
                    var dinner_id = data_obj.getString("dinner_address_id")

                    if (!lunch_id.equals("0")) {
                        lunch_obj = data_obj.getJSONObject("lunchAddress")
                    }
                    if (!dinner_id.equals("0")) {
                        dinner_obj = data_obj.getJSONObject("dinnerAddress")
                    }
                    var order_obj = data_obj.getJSONObject("orderDetail")
                    list.add(
                        ActivePackage(
                            id, lunch_id, dinner_id, lunch_obj,
                            dinner_obj, order_obj
                        )
                    )
                }
            }
            return list
        }

        // for choose menu
        fun getOrderDatelistfromToday(dateData: JSONArray): ArrayList<OrderDates> {
            var list = ArrayList<OrderDates>()
            if (dateData.length() > 0) {
                for (x in 0..dateData.length() - 1) {
                    var data_obj = dateData.getJSONObject(x)
                    var date = data_obj.getString("date")
                    if (MyCalendar.checkPresentDate(date)) {

                        var lunch_status = data_obj.getString("lunch_status")
                        var dinner_status = data_obj.getString("dinner_status")
                        var lunch_add_obj: JSONObject? = null
                        var dinner_add_obj: JSONObject? = null
                        if (lunch_status.equals("1")) {
                            lunch_add_obj = data_obj.getJSONObject("LunchAddress")
                        }
                        if (dinner_status.equals("1")) {
                            dinner_add_obj = data_obj.getJSONObject("DinnerAddress")
                        }
                        list.add(
                            OrderDates(
                                data_obj.getString("id"),
                                data_obj.getString("order_id"),
                                data_obj.getString("date"),
                                data_obj.getString("lunch_time"),
                                data_obj.getString("dinner_time"),
                                data_obj.getString("lunch_qty"),
                                data_obj.getString("dinner_qty"),
                                data_obj.getString("lunch_vendor_id"),
                                data_obj.getString("dinner_vendor_id"),
                                lunch_status, dinner_status,
                                data_obj.getString("lunch_address_id"),
                                data_obj.getString("dinner_address_id"),
                                data_obj.getString("package_name"),
                                lunch_add_obj,
                                dinner_add_obj,
                                data_obj.getString("package_id"),
                                data_obj.getString("tiffin_type")
                            )
                        )
                    }
                }
            }
            return list
        }

        fun getUserMenu(userMenuArray: JSONArray): String {
            var builder = StringBuilder()
            for (i in 0..userMenuArray.length() - 1) {
                var name = userMenuArray.getJSONObject(i).getString("item_name")
                builder?.append(name)
                if (i < userMenuArray.length() - 1) {
                    builder?.append(",")
                }
            }
            return builder.toString()
        }

        fun getOrderHistory(orderData: JSONArray): java.util.ArrayList<OrderHistory>? {
            var list = ArrayList<OrderHistory>()
            if (orderData.length() > 0) {
                for (x in 0..orderData.length() - 1) {
                    var data_obj = orderData.getJSONObject(x)
                    list.add(
                        OrderHistory(
                            data_obj.getString("id"),
                            data_obj.getString("package_name"),
                            data_obj.getString("date"),
                            data_obj.getString("lunch_qty"),
                            data_obj.getString("lunch_status"),
                            data_obj.getString("lunch_tiffin_status"),
                            data_obj.getString("dinner_qty"),
                            data_obj.getString("dinner_status"),
                            data_obj.getString("dinner_tiffin_status")
                        )
                    )
                }
            }
            return list
        }

        fun getfreeOrderHistory(orderData: JSONArray): ArrayList<OrderHistory>? {
            var list = ArrayList<OrderHistory>()
            if (orderData.length() > 0) {
                for (x in 0..orderData.length() - 1) {
                    var data_obj = orderData.getJSONObject(x)
                    var type = data_obj.getString("service_type")
                    if (type.equals("Lunch")) {
                        list.add(
                            OrderHistory(
                                data_obj.getString("id"),
                                "Free Dabba", data_obj.getString("order_date"),
                                "1", "1",
                                data_obj.getString("status"),
                                "0", "0", "0"
                            )
                        )
                    } else {
                        list.add(
                            OrderHistory(
                                data_obj.getString("id"),
                                "Free Dabba",
                                data_obj.getString("order_date"),
                                "0", "0", "0", "1",
                                "1", data_obj.getString("status")
                            )
                        )
                    }
                }
            }
            return list
        }

        fun getDiscount(discountArray: JSONArray): String {
            if (discountArray.length() > 0) {
                return discountArray.getJSONObject(0).getString("discount_percent")
            } else {
                return ""
            }
        }
    }
}