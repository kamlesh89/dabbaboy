package info.tech.dabbaboy.util

class WebApis {
    companion object {


        //        val BASE_URL = "http://lakesideayurveda.com/dabba_boy/Api/"
        val BASE_URL = "https://shreetechno.com/dabba_boy/Api/"
        val API_LOGIN = BASE_URL + "Login"
        val API_FORGOT = BASE_URL + "forgotPassword"
        val API_AREA = BASE_URL + "getAreaName"
        val API_SUBMIT_ORDER = BASE_URL + "orderSubmitionByUser"

        // update singal order time and address
        val API_UPDATE_SINGLE_ORDER = BASE_URL + "updateAddressTimeById"

        // update daily order
        val API_UPDATE_DAILY_ORDER = BASE_URL + "insertUpdateOrderDetail"

        // update start date of order
        val API_UPDATE_START_DATE = BASE_URL + "updateOrderDateByOrderId"

        // add extra tiffin
        val API_ADD_EXTRA = BASE_URL + "updateQuantityByRowId"

        // update order address
        val API_UPDATE_ORDER_ADDRESS = BASE_URL + "upadateAddressbyOrderId"

        // user profile
        val API_USER_PROFILE = BASE_URL + "getAllUserprofileDetail"

        // user address and order details
        val API_USER_ADDRESS_DETAILS = BASE_URL + "getUserAddressDetail"

        // vendors for user
        val API_VENDOR_BY_PINCODE = BASE_URL + "getVendorDetailByPincode"

        // submit menu and vendor
        val API_SUBMIT_MENU = BASE_URL + "InsertMenuOrderByUser"

        // get Order dates by order id
        val API_ORDER_DATES = BASE_URL + "getOrderDetailByorderId"

        // order history
        val API_ORDER_HISTORY = BASE_URL + "gethistoryOrderDetailForUser"

        // user profile
        val API_USE_PROFILE = BASE_URL + "getUserProfileData"

        // package
        @JvmField
        val API_PACKAGE = BASE_URL + "allHeaderDetail"

        // package price
        @JvmField
        val API_SERVICE_TIME = BASE_URL + "getServicesTime"

        // insert free dabba
        val API_ORDER_FREE_DABBA = BASE_URL + "insertFreeDabbatiffinDetail"

        // paytm checksum
        @JvmField
        val API_PAYTM_CHECKSUM = BASE_URL + "paymentChecksum"


        // address apis
        val API_ADD_ADDRESS = BASE_URL + "addNewAddress" // add , update , delete
        val API_GET_ADDRESS = BASE_URL + "getUserAddress" // get all address
    }
}