package info.tech.dabbaboy.util;


import android.app.Application;


public class MyApplication extends Application {
    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/amethysta_regular.ttf");
        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

}
