package info.tech.dabbaboy.util

import info.tech.dabbaboy.pojo.OrderDates
import org.json.JSONArray
import org.json.JSONObject

class ApiObjSchdule {
    companion object {

        // update lunch time
        fun updateLunchtime(id: String, time: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("id", id)
            obj.put("lunch_time", time)
            return obj
        }

        // update dinner time
        fun updateDinnertime(id: String, time: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "1")
            obj.put("id", id)
            obj.put("dinner_time", time)
            return obj
        }

        // update lunch address
        fun updateLunchAddress(id: String, add_id: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "2")
            obj.put("id", id)
            obj.put("lunch_address_id", add_id)
            return obj
        }

        // update dinner address
        fun updateDinnerAddress(id: String, add_id: String): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "3")
            obj.put("id", id)
            obj.put("dinner_address_id", add_id)
            return obj
        }

        // remove lunch
        fun removeLunchfromSchedule(
            order: OrderDates,
            lastorder: OrderDates,
            userid: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            var insert_arr = JSONArray()
            var update_arr = JSONArray()
            var delete_arr = JSONArray()

            // update
            var update_obj = JSONObject()
            update_obj.put("id", order.id)
            // lunch
            update_obj.put("lunch_time", "")
            update_obj.put("lunch_qty", "0")
            update_obj.put("lunch_vendor_id", "")
            update_obj.put("lunch_status", "0")
            update_obj.put("lunch_address_id", "")
            // dinner
            update_obj.put("dinner_time", order.dinner_time)
            update_obj.put("dinner_qty", order.dinner_qty)
            update_obj.put("dinner_vendor_id", order.dinner_vendor_id)
            update_obj.put("dinner_status", order.dinner_status)
            update_obj.put("dinner_address_id", order.dinner_add_id)
            update_arr.put(update_obj)


            // insert
            var insert_obj = JSONObject()
            insert_obj.put("order_id", order.order_id)
            insert_obj.put("user_id", userid)
            insert_obj.put("date", Utility.getnextDay(lastorder.date))
            // lunch
            insert_obj.put("lunch_time", order.lunch_time)
            insert_obj.put("lunch_qty", order.lunch_qty)
            insert_obj.put("lunch_vendor_id", "")
            insert_obj.put("lunch_status", order.lunch_status)
            insert_obj.put("lunch_address_id", order.lunch_add_id)
            // dinner
            insert_obj.put("dinner_qty", "0")
            insert_obj.put("dinner_time", "")
            insert_obj.put("dinner_vendor_id", "")
            insert_obj.put("dinner_status", "0")
            insert_obj.put("dinner_address_id", "")
            insert_obj.put("package_name", order.package_name)
            insert_obj.put("pack_id", order.pack_id)
            insert_obj.put("tiffin_type", order.packing_type)
            insert_arr.put(insert_obj)

            obj.put("insert_order", insert_arr)
            obj.put("update_order", update_arr)
            obj.put("delete_order", delete_arr)
            return obj
        }

        // add Lunch from Schedule
        fun addLunchfromSchedule(
            order: OrderDates,
            lastorder: OrderDates,
            userid: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            var insert_arr = JSONArray()
            var update_arr = JSONArray()
            var delete_arr = JSONArray()


            if (lastorder.lunch_status.equals("0") && lastorder.dinner_status.equals("1")) {
                var delete_obj = JSONObject()
                delete_obj.put("id", lastorder.id)
                delete_arr.put(delete_obj)
                //update lunch from dinner
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", "")
                update_obj.put("lunch_qty", lastorder.dinner_qty)
                update_obj.put("lunch_vendor_id", "")
                update_obj.put("lunch_status", lastorder.dinner_status)
                update_obj.put("lunch_address_id", lastorder.dinner_add_id)
                // dinner
                update_obj.put("dinner_time", order.dinner_time)
                update_obj.put("dinner_qty", order.dinner_qty)
                update_obj.put("dinner_vendor_id", order.dinner_vendor_id)
                update_obj.put("dinner_status", order.dinner_status)
                update_obj.put("dinner_address_id", order.dinner_add_id)
                update_arr.put(update_obj)

            } else if (lastorder.lunch_status.equals("1") && lastorder.dinner_status.equals("0")) {
                var delete_obj = JSONObject()
                delete_obj.put("id", lastorder.id)
                delete_arr.put(delete_obj)

                //update lunch from last order lunch
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", lastorder.lunch_time)
                update_obj.put("lunch_qty", lastorder.lunch_qty)
                update_obj.put("lunch_vendor_id", "")
                update_obj.put("lunch_status", lastorder.lunch_status)
                update_obj.put("lunch_address_id", lastorder.lunch_add_id)
                // dinner
                update_obj.put("dinner_time", order.dinner_time)
                update_obj.put("dinner_qty", order.dinner_qty)
                update_obj.put("dinner_vendor_id", order.dinner_vendor_id)
                update_obj.put("dinner_status", order.dinner_status)
                update_obj.put("dinner_address_id", order.dinner_add_id)
                update_arr.put(update_obj)

            } else {
                //update order lunch from last order dinner
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", "")
                update_obj.put("lunch_qty", lastorder.dinner_qty)
                update_obj.put("lunch_vendor_id", "")
                update_obj.put("lunch_status", lastorder.dinner_status)
                update_obj.put("lunch_address_id", lastorder.dinner_add_id)
                // dinner
                update_obj.put("dinner_time", order.dinner_time)
                update_obj.put("dinner_qty", order.dinner_qty)
                update_obj.put("dinner_vendor_id", order.dinner_vendor_id)
                update_obj.put("dinner_status", order.dinner_status)
                update_obj.put("dinner_address_id", order.dinner_add_id)
                update_arr.put(update_obj)

                //update last order dinner - off
                var update_obj1 = JSONObject()
                update_obj1.put("id", lastorder.id)
                // lunch
                update_obj1.put("lunch_time", lastorder.lunch_time)
                update_obj1.put("lunch_qty", lastorder.lunch_qty)
                update_obj1.put("lunch_vendor_id", lastorder.lunch_vendor_id)
                update_obj1.put("lunch_status", lastorder.lunch_status)
                update_obj1.put("lunch_address_id", lastorder.lunch_add_id)
                // dinner
                update_obj1.put("dinner_time", "")
                update_obj1.put("dinner_qty", "0")
                update_obj1.put("dinner_vendor_id", "")
                update_obj1.put("dinner_status", "0")
                update_obj1.put("dinner_address_id", "")
                update_arr.put(update_obj1)
            }
            obj.put("insert_order", insert_arr)
            obj.put("update_order", update_arr)
            obj.put("delete_order", delete_arr)
            return obj
        }

        // remove Dinner from Schedule
        fun removeDinnerfromSchedule(
            order: OrderDates,
            lastorder: OrderDates,
            userid: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            var insert_arr = JSONArray()
            var update_arr = JSONArray()
            var delete_arr = JSONArray()

            // update
            var update_obj = JSONObject()
            update_obj.put("id", order.id)
            // lunch
            update_obj.put("lunch_time", order.lunch_time)
            update_obj.put("lunch_qty", order.lunch_qty)
            update_obj.put("lunch_vendor_id", order.lunch_vendor_id)
            update_obj.put("lunch_status", order.lunch_status)
            update_obj.put("lunch_address_id", order.lunch_add_id)
            // dinner
            update_obj.put("dinner_time", "")
            update_obj.put("dinner_qty", "0")
            update_obj.put("dinner_vendor_id", "")
            update_obj.put("dinner_status", "0")
            update_obj.put("dinner_address_id", "")
            update_arr.put(update_obj)

            // update last order if dinner not available
            if (lastorder.dinner_status.equals("0")) {
                var update_obj = JSONObject()
                update_obj.put("id", lastorder.id)
                // lunch
                update_obj.put("lunch_time", lastorder.lunch_time)
                update_obj.put("lunch_qty", lastorder.lunch_qty)
                update_obj.put("lunch_vendor_id", lastorder.lunch_vendor_id)
                update_obj.put("lunch_status", lastorder.lunch_status)
                update_obj.put("lunch_address_id", lastorder.lunch_add_id)
                // dinner
                update_obj.put("dinner_time", order.dinner_time)
                update_obj.put("dinner_qty", order.dinner_qty)
                update_obj.put("dinner_vendor_id", "")
                update_obj.put("dinner_status", order.dinner_status)
                update_obj.put("dinner_address_id", order.dinner_add_id)
                update_arr.put(update_obj)
            } else {
                var insert_obj = JSONObject()
                insert_obj.put("order_id", order.order_id)
                insert_obj.put("user_id", userid)
                insert_obj.put("date", Utility.getnextDay(lastorder.date))
                // lunch
                insert_obj.put("lunch_time", "")
                insert_obj.put("lunch_qty", "0")
                insert_obj.put("lunch_vendor_id", "")
                insert_obj.put("lunch_status", "0")
                insert_obj.put("lunch_address_id", "")
                // dinner
                insert_obj.put("dinner_qty", order.dinner_qty)
                insert_obj.put("dinner_time", order.dinner_time)
                insert_obj.put("dinner_vendor_id", "")
                insert_obj.put("dinner_status", order.dinner_status)
                insert_obj.put("dinner_address_id", order.dinner_add_id)
                insert_obj.put("package_name", order.package_name)
                insert_obj.put("pack_id", order.pack_id)
                insert_obj.put("tiffin_type", order.packing_type)
                insert_arr.put(insert_obj)
            }
            obj.put("insert_order", insert_arr)
            obj.put("update_order", update_arr)
            obj.put("delete_order", delete_arr)
            return obj
        }


        // add Dinner from Schedule
        fun addDinnerfromSchedule(
            order: OrderDates,
            lastorder: OrderDates,
            userid: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            var insert_arr = JSONArray()
            var update_arr = JSONArray()
            var delete_arr = JSONArray()

            if (lastorder.lunch_status.equals("0") && lastorder.dinner_status.equals("1")) {
                var delete_obj = JSONObject()
                delete_obj.put("id", lastorder.id)
                delete_arr.put(delete_obj)
                //update dinner from dinner
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", order.lunch_time)
                update_obj.put("lunch_qty", order.lunch_qty)
                update_obj.put("lunch_vendor_id", order.lunch_vendor_id)
                update_obj.put("lunch_status", order.lunch_status)
                update_obj.put("lunch_address_id", order.lunch_add_id)
                // dinner
                update_obj.put("dinner_time", lastorder.dinner_time)
                update_obj.put("dinner_qty", lastorder.dinner_qty)
                update_obj.put("dinner_vendor_id", "")
                update_obj.put("dinner_status", lastorder.dinner_status)
                update_obj.put("dinner_address_id", lastorder.dinner_add_id)
                update_arr.put(update_obj)

            } else if (lastorder.lunch_status.equals("1") && lastorder.dinner_status.equals("0")) {
                var delete_obj = JSONObject()
                delete_obj.put("id", lastorder.id)
                delete_arr.put(delete_obj)

                //update dinner from last order lunch
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", order.lunch_time)
                update_obj.put("lunch_qty", order.lunch_qty)
                update_obj.put("lunch_vendor_id", order.lunch_vendor_id)
                update_obj.put("lunch_status", order.lunch_status)
                update_obj.put("lunch_address_id", order.lunch_add_id)
                // dinner
                update_obj.put("dinner_time", "")
                update_obj.put("dinner_qty", lastorder.lunch_qty)
                update_obj.put("dinner_vendor_id", "")
                update_obj.put("dinner_status", lastorder.lunch_status)
                update_obj.put("dinner_address_id", lastorder.lunch_add_id)
                update_arr.put(update_obj)

            } else {
                //update order dinner from last order dinner
                var update_obj = JSONObject()
                update_obj.put("id", order.id)
                // lunch
                update_obj.put("lunch_time", order.lunch_time)
                update_obj.put("lunch_qty", order.lunch_qty)
                update_obj.put("lunch_vendor_id", order.lunch_vendor_id)
                update_obj.put("lunch_status", order.lunch_status)
                update_obj.put("lunch_address_id", order.lunch_add_id)
                // dinner
                update_obj.put("dinner_time", lastorder.dinner_time)
                update_obj.put("dinner_qty", lastorder.dinner_qty)
                update_obj.put("dinner_vendor_id", "")
                update_obj.put("dinner_status", lastorder.dinner_status)
                update_obj.put("dinner_address_id", lastorder.dinner_add_id)
                update_arr.put(update_obj)

                //update last order dinner - off
                var update_obj1 = JSONObject()
                update_obj1.put("id", lastorder.id)
                // lunch
                update_obj1.put("lunch_time", lastorder.lunch_time)
                update_obj1.put("lunch_qty", lastorder.lunch_qty)
                update_obj1.put("lunch_vendor_id", lastorder.lunch_vendor_id)
                update_obj1.put("lunch_status", lastorder.lunch_status)
                update_obj1.put("lunch_address_id", lastorder.lunch_add_id)
                // dinner
                update_obj1.put("dinner_time", "")
                update_obj1.put("dinner_qty", "0")
                update_obj1.put("dinner_vendor_id", "")
                update_obj1.put("dinner_status", "0")
                update_obj1.put("dinner_address_id", "")
                update_arr.put(update_obj1)
            }
            obj.put("insert_order", insert_arr)
            obj.put("update_order", update_arr)
            obj.put("delete_order", delete_arr)
            return obj
        }


        // buy extra tiffin objects

        // add lunch
        fun addextraLunch(
            orderDates: OrderDates,
            lastOrder: OrderDates,
            row1_qty: String
        ): JSONObject {

            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("id1", orderDates.id)
            obj.put("service_type1", "Lunch")
            obj.put("quantity1", row1_qty)

            // last order
            obj.put("id2", lastOrder.id)
            obj.put("service_type2", getRow2Servicetype(lastOrder))
            obj.put("quantity2", getRow2Quantity(lastOrder))
            obj.put("status2", getRow2Status(lastOrder))
            return obj
        }

        // remove lunch
        fun removeextraLunch(
            orderDates: OrderDates,
            lastOrder: OrderDates,
            qty: String,
            user_id: String
        ): JSONObject {

            // update
            var obj = JSONObject()
            obj.put("mode", "1")
            obj.put("id", orderDates.id)
            obj.put("service_type", "Lunch")
            obj.put("quantity", qty)

            // insert new after last order
            var insert_arr = JSONArray()
            var insert_obj = JSONObject()
            insert_obj.put("order_id", lastOrder.order_id)
            insert_obj.put("user_id", user_id)
            insert_obj.put("date", Utility.getnextDay(lastOrder.date))
            // lunch
            insert_obj.put("lunch_qty", "1")
            insert_obj.put("lunch_vendor_id", "")
            insert_obj.put("lunch_status", "1")

            if (lastOrder.lunch_status.equals("1")) {
                insert_obj.put("lunch_time", lastOrder.lunch_time)
                insert_obj.put("lunch_address_id", lastOrder.lunch_add_id)
            } else {
                insert_obj.put("lunch_time", lastOrder.dinner_time)
                insert_obj.put("lunch_address_id", lastOrder.dinner_add_id)
            }
            // dinner
            insert_obj.put("dinner_qty", "0")
            insert_obj.put("dinner_time", "")
            insert_obj.put("dinner_vendor_id", "")
            insert_obj.put("dinner_status", "0")
            insert_obj.put("dinner_address_id", "")
            insert_obj.put("package_name", orderDates.package_name)
            insert_obj.put("pack_id", orderDates.pack_id)
            insert_obj.put("tiffin_type", orderDates.packing_type)
            insert_arr.put(insert_obj)

            obj.put("insert_order", insert_arr)
            return obj
        }

        // remove extra dinner
        fun removeextraDinner(
            orderDates: OrderDates,
            lastOrder: OrderDates,
            qty: String,
            user_id: String
        ): JSONObject {

            // update
            var obj = JSONObject()
            obj.put("mode", "1")
            obj.put("id", orderDates.id)
            obj.put("service_type", "Dinner")
            obj.put("quantity", qty)

            // insert new after last order
            var insert_arr = JSONArray()
            var insert_obj = JSONObject()
            insert_obj.put("order_id", lastOrder.order_id)
            insert_obj.put("user_id", user_id)
            insert_obj.put("date", Utility.getnextDay(lastOrder.date))
            // lunch
            insert_obj.put("lunch_time", "")
            insert_obj.put("lunch_qty", "0")
            insert_obj.put("lunch_vendor_id", "")
            insert_obj.put("lunch_status", "0")
            insert_obj.put("lunch_address_id", "")
            // dinner
            insert_obj.put("dinner_qty", "1")
            insert_obj.put("dinner_vendor_id", "")
            insert_obj.put("dinner_status", "1")

            if (lastOrder.dinner_status.equals("1")) {
                insert_obj.put("dinner_time", lastOrder.dinner_time)
                insert_obj.put("dinner_address_id", lastOrder.dinner_add_id)
            } else {
                insert_obj.put("dinner_time", lastOrder.lunch_time)
                insert_obj.put("dinner_address_id", lastOrder.lunch_add_id)
            }

            insert_obj.put("package_name", orderDates.package_name)
            insert_obj.put("pack_id", orderDates.pack_id)
            insert_obj.put("tiffin_type", orderDates.packing_type)
            insert_arr.put(insert_obj)

            obj.put("insert_order", insert_arr)
            return obj
        }

        // add dinner
        fun addextraDinner(
            orderDates: OrderDates,
            lastOrder: OrderDates,
            row1_qty: String
        ): JSONObject {
            var obj = JSONObject()
            obj.put("mode", "0")
            obj.put("id1", orderDates.id)
            obj.put("service_type1", "Dinner")
            obj.put("quantity1", row1_qty)

            // last order
            obj.put("id2", lastOrder.id)
            obj.put("service_type2", getRow2Servicetype(lastOrder))
            obj.put("quantity2", getRow2Quantity(lastOrder))
            obj.put("status2", getRow2Status(lastOrder))
            return obj
        }


        private fun getRow2Status(lastOrder: OrderDates): String {
            if (lastOrder.lunch_status.equals("1") && lastOrder.dinner_status.equals("1")) {
                return "0"
            } else {
                return ""
            }
        }

        private fun getRow2Quantity(lastOrder: OrderDates): String {
            if (lastOrder.lunch_status.equals("1") && lastOrder.dinner_status.equals("1")) {
                return "0"
            } else {
                return ""
            }
        }

        private fun getRow2Servicetype(lastOrder: OrderDates): String {
            if (lastOrder.lunch_status.equals("1") && lastOrder.dinner_status.equals("1")) {
                return "Dinner"
            } else {
                return "Delete"
            }
        }

    }
}