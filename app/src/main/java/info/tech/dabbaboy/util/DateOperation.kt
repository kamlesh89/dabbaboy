package info.tech.dabbaboy.util

import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class DateOperation {
    companion object {

        fun getSubmitAllDate(
            lunch: String,
            dinner: String,
            startdate: String,
            lunchAddressId: String?,
            dinnerAddressId: String?,
            packName: String,
            qty: String,
            package_id: String,
            tiffinType: String
        ): JSONArray {

            var quantity: Int = qty.toInt()
            var remainder: Int = 0
            var no_day: Int = quantity;
            var lunch_qty = 1;
            var dinner_qty = 1;
            if (lunch.equals("yes") && dinner.equals("yes")) {
                remainder = quantity % 2
                no_day = quantity / 2 + remainder
            } else if (lunch.equals("yes") && dinner.equals("no")) {
                no_day = quantity
                dinner_qty = 0
            } else if (lunch.equals("no") && dinner.equals("yes")) {
                no_day = quantity
                lunch_qty = 0
            }
            return getAllDate(
                no_day, startdate, lunch_qty.toString(), dinner_qty.toString(),
                lunchAddressId!!, dinnerAddressId!!, packName, remainder, package_id, tiffinType
            )
        }

        private fun getAllDate(
            noDay: Int, startdate: String,
            lunch_qty: String, dinner_qty: String,
            lunch_add_id: String, dinner_add_id: String,
            pack_name: String,
            remainder: Int,
            package_id: String,
            tiffinType: String
        ): JSONArray {

            var data = JSONArray()
            val date_format = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            for (i in 0..noDay - 1) {

                val date = date_format.parse(startdate)
                val calendar = Calendar.getInstance()
                calendar.time = date
                calendar.add(Calendar.DAY_OF_YEAR, i)
                val day2 = calendar.time
                val new_date: String = date_format.format(day2)
                // object
                if (i == noDay - 1 && remainder == 1) {
                    var loop_obj = JSONObject()
                    loop_obj.put("date", new_date)
                    loop_obj.put("lunch_qty", lunch_qty)
                    loop_obj.put("dinner_qty", "0")
                    loop_obj.put("lunch_status", lunch_qty)
                    loop_obj.put("dinner_status", "0")
                    loop_obj.put("lunch_address_id", lunch_add_id)
                    loop_obj.put("dinner_address_id", "")
                    loop_obj.put("pack_name", pack_name)
                    loop_obj.put("pack_id", package_id)
                    loop_obj.put("tiffin_type", tiffinType)
                    data.put(loop_obj)
                } else {
                    var loop_obj = JSONObject()
                    loop_obj.put("date", new_date)
                    loop_obj.put("lunch_qty", lunch_qty)
                    loop_obj.put("dinner_qty", dinner_qty)
                    loop_obj.put("lunch_status", lunch_qty)
                    loop_obj.put("dinner_status", dinner_qty)
                    loop_obj.put("lunch_address_id", lunch_add_id)
                    loop_obj.put("dinner_address_id", dinner_add_id)
                    loop_obj.put("pack_name", pack_name)
                    loop_obj.put("pack_id", package_id)
                    loop_obj.put("tiffin_type", tiffinType)
                    data.put(loop_obj)
                }
            }
            return data
        }
    }
}