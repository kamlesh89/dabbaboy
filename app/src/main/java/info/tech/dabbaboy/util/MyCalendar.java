package info.tech.dabbaboy.util;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MyCalendar {

    public static String getMonthName(String month) {
        if (month.equals("01")) {
            return "January";
        } else if (month.equals("02")) {
            return "February";
        } else if (month.equals("03")) {
            return "March";
        } else if (month.equals("04")) {
            return "April";
        } else if (month.equals("05")) {
            return "May";
        } else if (month.equals("06")) {
            return "June";
        } else if (month.equals("07")) {
            return "July";
        } else if (month.equals("08")) {
            return "August";
        } else if (month.equals("09")) {
            return "September";
        } else if (month.equals("10")) {
            return "October";
        } else if (month.equals("11")) {
            return "November";
        } else {
            return "December";
        }
    }

    @NotNull
    public static String getMonthNumber(@NotNull String month_name) {
        if (month_name.equals("January")) {
            return "01";
        } else if (month_name.equals("February")) {
            return "02";
        } else if (month_name.equals("March")) {
            return "03";
        } else if (month_name.equals("April")) {
            return "04";
        } else if (month_name.equals("May")) {
            return "05";
        } else if (month_name.equals("June")) {
            return "06";
        } else if (month_name.equals("July")) {
            return "07";
        } else if (month_name.equals("August")) {
            return "08";
        } else if (month_name.equals("September")) {
            return "09";
        } else if (month_name.equals("October")) {
            return "10";
        } else if (month_name.equals("November")) {
            return "11";
        } else {
            return "12";
        }
    }

    public static final boolean checkPresentDate(String date) {

        Date strDate = null, curr_day = null;
        // today date
        Calendar cal = Calendar.getInstance();
        Date today_date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today_string = sdf.format(today_date);

        try {
            strDate = sdf.parse(date);
            curr_day = sdf.parse(today_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (strDate.getTime() >= curr_day.getTime() || date.equals(today_string)) {
            return true;
        } else {
            return false;
        }
    }
}
