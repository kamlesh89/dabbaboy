package info.tech.dabbaboy.activity.subscription

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.adapter.MySpinnerAdapter
import info.tech.dabbaboy.pojo.Varient
import info.tech.dabbaboy.pojo.VendorPackage
import info.tech.dabbaboy.util.*
import info.tech.dabbaboy.util.Utility.Companion.hideLoader
import info.tech.dabbaboy.util.Utility.Companion.showLoader
import info.tech.dabbaboy.util.Utility.Companion.snacbarShow
import kotlinx.android.synthetic.main.activity_packagedetails.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class PackageDeatilsActivity : AppCompatActivity(), View.OnClickListener,
    CompoundButton.OnCheckedChangeListener {

    lateinit var ctx: Context
    lateinit var pack: VendorPackage
    lateinit var sessionManager: SessionManager
    lateinit var varient_list: ArrayList<Varient>
    var address_lunch_id: String? = ""
    var address_dinner_id: String? = ""
    var qty = ""
    var discount = ""

    val INTENT_SELECT_ADDRESS_LUNCH = 101
    val INTENT_SELECT_ADDRESS_DINNER = 102

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packagedetails)
        initXml()
        if (ConnectionDetector.isConnected()) {
            loadTimimg(ApiObjects.getMode("0"))
        } else {
            snacbarShow(ctx.getString(R.string.no_internet), coordinator_packagedetail)
        }
    }

    private fun initXml() {
        ctx = this
        tv_header_title.setText(ctx.getText(R.string.title_addpackage))
        tv_addpack_date.setOnClickListener(this)
        tv_addpack_addresslunch.setOnClickListener(this)
        tv_addpack_addressdinner.setOnClickListener(this)
        bt_addpack_continue.setOnClickListener(this)
        iv_header_back.setOnClickListener(this)
        bt_addpack_applydis.setOnClickListener(this)

        cb_addpack_lunch.setOnCheckedChangeListener(this)
        cb_addpack_dinner.setOnCheckedChangeListener(this)
        rb_addpack_hot.setOnCheckedChangeListener(this)
        rb_addpack_disposal.setOnCheckedChangeListener(this)

        pack = intent.getParcelableExtra("data")

        sessionManager = SessionManager(ctx)
        setAlldata()
    }

    private fun loadTimimg(mode: JSONObject) {
        showLoader(ctx)
        AndroidNetworking.post(WebApis.API_SERVICE_TIME)
            .addJSONObjectBody(mode)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    hideLoader()
                    setTimeResponse(response)
                }

                override fun onError(anError: ANError) {
                    hideLoader()
                }
            })
    }

    private fun setTimeResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            if (status == 200) {
                val data_obj = response.getJSONObject("data")
                var lunch_array = data_obj.getJSONArray("Lunch")
                var dinner_array = data_obj.getJSONArray("Dinner")
                var discount_array = data_obj.getJSONArray("discount_data")
                var lunch_list = JsonParser.gettimelist(lunch_array)
                var dinner_list = JsonParser.gettimelist(dinner_array)
                discount = JsonParser.getDiscount(discount_array)
                setTimspinner(lunch_list, dinner_list)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setTimspinner(lunchList: List<String>, dinnerList: List<String>) {
        sp_addpack_lunchtime.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                lunchList
            )
        )

        sp_addpack_dinnertime.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                dinnerList
            )
        )
    }


    private fun setAlldata() {

        Glide.with(ctx)
            .load(pack.getPackage_image().replace(" ", "%20"))
            .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(10)))
            .into(iv_addpack_image)
        tv_addpack_mealname.setText(pack.package_name)

        tv_addpack_mealsetail.setText(intent.getStringExtra("menu"))
        varient_list = JsonParser.getVarientlist(JSONArray(pack.package_var_array))
        val var_adapter = MySpinnerAdapter(
            this,
            R.layout.adp_spinnerview,
            R.id.tv_adp_nav,
            varient_list
        )
        sp_addpack_qty.setAdapter(var_adapter)
        sp_addpack_qty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                qty = varient_list[position].days
                tv_addpack_packamount.setText(varient_list[position].price)
                setTotal(true)
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.iv_header_back -> {
                finish()
            }

            R.id.tv_addpack_date -> {
                showCalendar()
            }

            R.id.bt_addpack_applydis -> {
                applyDiscount()
                setTotal(false)
            }


            R.id.tv_addpack_addresslunch -> {
                startActivityForResult(
                    Intent(ctx, AddressActivity::class.java),
                    INTENT_SELECT_ADDRESS_LUNCH
                )
            }

            R.id.tv_addpack_addressdinner -> {
                startActivityForResult(
                    Intent(ctx, AddressActivity::class.java),
                    INTENT_SELECT_ADDRESS_DINNER
                )
            }

            R.id.bt_addpack_continue -> {
                if (isValid()) {
                    selectPaymentMethod()
                }
            }

        }
    }

    private fun applyDiscount() {
        if (discount.equals("")) {
            snacbarShow("No Offer Available", coordinator_packagedetail)
        } else {
            var pack_amount = tv_addpack_packamount.text.toString().toInt()
            var disc_percent = discount.toInt()
            var discount_amount = pack_amount * disc_percent / 100
            tv_addpack_discount.text = discount_amount.toString()
            snacbarShow("Offer Applied. ( $discount % OFF )", coordinator_packagedetail)
        }
    }

    private fun getTiffintype(): String {
        if (rb_addpack_hot.isChecked) {
            return "Hot Dabba"
        } else if (rb_addpack_disposal.isChecked) {
            return "Disposal Dabba"
        } else {
            return ""
        }
    }

    private fun getLunch(): String {
        if (cb_addpack_lunch.isChecked) {
            return "yes"
        } else {
            return "no"
        }
    }

    private fun getLunchtime(): String {
        if (getLunch().equals("yes")) {
            return sp_addpack_lunchtime.selectedItem.toString()
        } else {
            return ""
        }
    }

    private fun getDinner(): String {
        if (cb_addpack_dinner.isChecked) {
            return "yes"
        } else {
            return "no"
        }
    }

    private fun getDinnertime(): String {
        if (getDinner().equals("yes")) {
            return sp_addpack_dinnertime.selectedItem.toString()
        } else {
            return ""
        }
    }

    private fun submitOrder(paytype: String) {
        showLoader(ctx)
        AndroidNetworking.post(WebApis.API_SUBMIT_ORDER)
            .addJSONObjectBody(getBody(paytype))
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    hideLoader()
                }
            })
    }

    private fun getBody(paytype: String): JSONObject? {
        var body = ApiObjects.getSubmitOrder(
            sessionManager.getData(SessionManager.KEY_ID),
            pack.package_id,
            qty,
            tv_addpack_date.text.toString(),
            getLunch(),
            getDinner(),
            getLunchtime(),
            getDinnertime(),
            address_lunch_id,
            address_dinner_id,
            tv_addpack_packamount.text.toString(),
            paytype,
            tv_addpack_mealname.text.toString(),
            getTiffintype(),
            tv_addpack_charges.text.toString(),
            tv_addpack_discount.text.toString()
        )
        return body
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            snacbarShow(message, coordinator_packagedetail)
            if (status == 200) {
                finishAffinity()
                startActivity(Intent(ctx, MainActivity::class.java))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            snacbarShow(ctx.getString(R.string.no_internet), coordinator_packagedetail)
            return false
        } else if (tv_addpack_date.text.toString().isEmpty()) {
            snacbarShow("Please select Start date", coordinator_packagedetail)
            return false
        } else if (getLunch().equals("no") && getDinner().equals("no")) {
            snacbarShow("Please select Atleast one from Lunch or Dinner", coordinator_packagedetail)
            return false
        } else if (getLunch().equals("yes") && sp_addpack_lunchtime.selectedItemPosition == 0) {
            snacbarShow("Please select Lunch time", coordinator_packagedetail)
            return false
        } else if (getDinner().equals("yes") && sp_addpack_dinnertime.selectedItemPosition == 0) {
            snacbarShow("Please select Dinner time", coordinator_packagedetail)
            return false
        } else if (getLunch().equals("yes") && tv_addpack_addresslunch.text.toString().isEmpty()) {
            snacbarShow("Please select Lunch Delivery Address", coordinator_packagedetail)
            return false
        } else if (getDinner().equals("yes") && tv_addpack_addressdinner.text.toString()
                .isEmpty()
        ) {
            snacbarShow("Please select Dinner Delivery Address", coordinator_packagedetail)
            return false
        } else if (!checkPacking()) {
            snacbarShow("Please select Packing", coordinator_packagedetail)
            return false
        }
        return true
    }

    private fun checkPacking(): Boolean {
        if (rb_addpack_hot.isChecked) {
            return true
        } else if (rb_addpack_disposal.isChecked) {
            return true
        } else {
            return false
        }
    }

    private fun showCalendar() {
        val date: String = Utility.getDate()
        val mDay = date.substring(8, 10).toInt()
        val mMonth = date.substring(5, 7).toInt() - 1
        val mYear = date.substring(0, 4).toInt()
        val datePickerDialog = DatePickerDialog(
            ctx,
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var monthOfYear = monthOfYear
                monthOfYear = monthOfYear + 1
                var day1 = dayOfMonth.toString()
                var month1 = monthOfYear.toString()
                if (day1.length == 1) {
                    day1 = "0$day1"
                }
                if (month1.length == 1) {
                    month1 = "0$month1"
                }
                tv_addpack_date.setText("$day1-$month1-$year")
            }, mYear, mMonth, mDay
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        when (p0?.id) {

            R.id.cb_addpack_lunch -> {
                if (p1) {
                    sp_addpack_lunchtime.visibility = View.VISIBLE
                    sp_addpack_lunchtime.selectedItemPosition == 0
                    ll_addpack_lunchview.visibility = View.VISIBLE
                } else {
                    sp_addpack_lunchtime.visibility = View.GONE
                    ll_addpack_lunchview.visibility = View.GONE
                    address_lunch_id = ""
                }
            }
            R.id.cb_addpack_dinner -> {
                if (p1) {
                    sp_addpack_dinnertime.visibility = View.VISIBLE
                    sp_addpack_dinnertime.selectedItemPosition == 0
                    ll_addpack_dinnerview.visibility = View.VISIBLE
                } else {
                    sp_addpack_dinnertime.visibility = View.GONE
                    ll_addpack_dinnerview.visibility = View.GONE
                    address_dinner_id = ""
                }
            }
            R.id.rb_addpack_hot -> {
                setTotal(false)
            }
            R.id.rb_addpack_disposal -> {
                setTotal(false)
            }
        }
    }

    private fun setTotal(checkDis: Boolean) {

        if (rb_addpack_hot.isChecked) {
            tv_addpack_charges.text = "600"
        } else if (rb_addpack_disposal.isChecked) {
            var charges = qty.toInt() * 12
            tv_addpack_charges.text = charges.toString()
        }

        if (checkDis) {
            if (!tv_addpack_discount.text.toString().equals("0")) {
                var pack_amount = tv_addpack_packamount.text.toString().toInt()
                var disc_percent = discount.toInt()
                var discount_amount = pack_amount * disc_percent / 100
                tv_addpack_discount.text = discount_amount.toString()
            }
        }

        var pack_amount = tv_addpack_packamount.text.toString().toInt()
        var charges_amount = tv_addpack_charges.text.toString().toInt()
        var discount_amount = tv_addpack_discount.text.toString().toInt()

        var total = pack_amount + charges_amount - discount_amount
        tv_addpack_amount.text = total.toString()
    }


    // payment dialog
    private fun selectPaymentMethod() {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_payment)
        dialog.getWindow()?.setLayout(
            ((Utility.getWidth(ctx) / 100) * 90),
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        var online_rb = dialog.findViewById<RadioButton>(R.id.rb_dialogpay_online)
        var cod_rb = dialog.findViewById<RadioButton>(R.id.rb_dialogpay_cod)
        var done_bt = dialog.findViewById<Button>(R.id.bt_dialogpay_done)
        done_bt.setOnClickListener(View.OnClickListener {
            if (online_rb.isChecked) {
                dialog.dismiss()
                startActivityForResult(
                    Intent(ctx, PaymentActivity::class.java).putExtra(
                        "amount",
                        tv_addpack_amount.text.toString()
                    ),
                    Constant.INTENT_PAYMENT
                )
            } else if (cod_rb.isChecked) {
                dialog.dismiss()
                submitOrder("COD")
            } else {

            }
        })
        dialog.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INTENT_SELECT_ADDRESS_LUNCH && resultCode == Activity.RESULT_OK) {
            address_lunch_id = data?.getStringExtra("id")
            tv_addpack_addresslunch.setText(data?.getStringExtra("address"))
        }
        if (requestCode == INTENT_SELECT_ADDRESS_DINNER && resultCode == Activity.RESULT_OK) {
            address_dinner_id = data?.getStringExtra("id")
            tv_addpack_addressdinner.setText(data?.getStringExtra("address"))
        }
        if (requestCode == Constant.INTENT_PAYMENT && resultCode == Activity.RESULT_OK) {
            submitOrder("Online")
        }
    }
}
