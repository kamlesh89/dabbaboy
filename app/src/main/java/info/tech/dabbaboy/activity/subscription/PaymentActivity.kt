package info.tech.dabbaboy.activity.subscription

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.SessionManager
import info.tech.dabbaboy.util.Utility
import org.json.JSONObject

class PaymentActivity : AppCompatActivity(), PaymentResultListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paytm_payment)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        startPayment()
    }

    private fun startPayment() {
        var amount = intent.getStringExtra("amount") + "00"
        var name = sessionManager.getData(SessionManager.KEY_NAME)

        val activity: Activity = this
        val co = Checkout()
        co.setKeyID("rzp_live_5ijznBBJRRa4bB")
        try {
            val options = JSONObject()
            options.put("name", name)
            options.put("description", "Tiffin Subscription")
            options.put("currency", "INR");
            options.put("amount", "500")//pass amount in currency subunits
            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(p0: Int, p1: String?) {
        Toast.makeText(this, "payment Cancelled", Toast.LENGTH_LONG).show()
        finish()
    }

    override fun onPaymentSuccess(p0: String?) {
//        var res_data = JSONObject(p0)
        // for live
//        var payment_id = res_data.getString("razorpay_payment_id")

        // for test
//        var payment_id = p0
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }
}
