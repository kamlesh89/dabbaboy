package info.tech.dabbaboy.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.subscription.MapsActivity
import info.tech.dabbaboy.pojo.Address
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_add_address.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class UpdateAddressActivity : BaseActivity(), View.OnClickListener, OnMapReadyCallback {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager
    lateinit var arealist: ArrayList<String>

    var lati: Double = 0.0
    var longi: Double = 0.0
    private var mMap: GoogleMap? = null
    val MAP_ACTIVITY = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_add_address
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        tv_header_title.setText("Edit Address")

        iv_header_back.setOnClickListener(this)
        met_addaddress_pincode.addTextChangedListener(textWatcher)
        bt_addaddress_proceed.setOnClickListener(this)
        setSpinner()

        // set map
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        // set data
        var address = intent.getParcelableExtra<Address>("data")
        met_addaddress_house.setText(address.house_no)
        met_addaddress_street.setText(address.street)
        met_addaddress_landmark.setText(address.landmark)
        met_addaddress_pincode.setText(address.pincode)
        lati = address.latitude!!
        longi = address.longitude!!

        fl_addaddress_map.setOnTouchListener { view, motionEvent ->
            startActivityForResult(
                Intent(ctx, MapsActivity::class.java)
                    .putExtra("lati", lati)
                    .putExtra("longi", longi)
                , MAP_ACTIVITY
            )
            false
        }

    }

    private fun setSpinner() {
        sp_addaddress_type.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(*resources.getStringArray(R.array.select_address_type))
            )
        )
    }

    private fun setareaSpinner() {
        sp_addaddress_area.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                arealist
            )
        )
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.iv_header_back -> {
                finish()
            }
            R.id.bt_addaddress_proceed -> {
                if (isVald()) {
                    submitAddress(
                        ApiObjects.updateAddress(
                            sp_addaddress_type.selectedItem.toString(),
                            met_addaddress_house.text.toString(),
                            met_addaddress_street.text.toString(),
                            met_addaddress_landmark.text.toString(),
                            met_addaddress_pincode.text.toString(),
                            sp_addaddress_area.selectedItem.toString(),
                            met_addaddress_city.text.toString(),
                            met_addaddress_state.text.toString(),
                            SessionManager(ctx).getData(SessionManager.KEY_ID),
                            getAddressid(),
                            lati.toString(), longi.toString()
                        )
                    )
                }
            }

        }
    }

    private fun getAddressid(): String {
        var address = intent.getParcelableExtra<Address>("data")
        return address.id!!
    }

    var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
            if (met_addaddress_pincode.text.toString().length == 6) {
                if (ConnectionDetector.isConnected()) {
                    Utility.hideKeyboard(ctx as UpdateAddressActivity)
                    submitPincode(
                        ApiObjects.getArea(
                            met_addaddress_pincode.text.toString()
                        )
                    )
                } else {
                    Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_addaddress)
                }
            } else {
                arealist = ArrayList<String>()
                arealist.add("Select Area")
                setareaSpinner()
            }
        }
    }

    private fun submitPincode(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_AREA)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setpincodeResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setpincodeResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var data_array = response.getJSONArray("data")
            arealist = JsonParser.getAreaList(data_array)
            setareaSpinner()
        } else {
            met_addaddress_pincode.setError("we are not serving in this area")
            Utility.snacbarShow(message, coordinator_addaddress)
        }
    }


    private fun submitAddress(apibody: JSONObject) {

        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ADD_ADDRESS)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getString("status")
            val message = info_obj.getString("message")
            if (status.equals("200")) {
                returncreatedAddress()
            } else {
                Utility.snacbarShow(message, coordinator_addaddress)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }


    private fun isVald(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_addaddress)
            return false
        } else if (met_addaddress_house.text.toString().isEmpty()) {
            met_addaddress_house.setError("Please enter House no.")
            met_addaddress_house.requestFocus()
            return false
        } else if (met_addaddress_street.text.toString().isEmpty()) {
            met_addaddress_street.setError("Please enter Street")
            met_addaddress_street.requestFocus()
            return false
        } else if (met_addaddress_landmark.text.toString().isEmpty()) {
            met_addaddress_landmark.setError("Please enter Landmark")
            met_addaddress_landmark.requestFocus()
            return false
        } else if (met_addaddress_pincode.text.toString().length != 6) {
            met_addaddress_pincode.setError("Please enter valid Pincode")
            met_addaddress_pincode.requestFocus()
            return false
        } else if (sp_addaddress_type.selectedItemPosition == 0) {
            Utility.snacbarShow("Please select Address Type", coordinator_addaddress)
            return false
        } else if (sp_addaddress_area.selectedItemPosition == 0) {
            Utility.snacbarShow("Please select Area", coordinator_addaddress)
            return false
        } else if (lati == 0.0) {
            Utility.snacbarShow("Please map your address Location", coordinator_addaddress)
            return false
        }
        return valid;
    }

    fun returncreatedAddress() {
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0

        var address = intent.getParcelableExtra<Address>("data")
        var lati = address.latitude
        var longi = address.longitude
        try {
            val sydney = LatLng(lati!!, longi!!)
            mMap!!.addMarker(
                MarkerOptions().position(sydney).title(getAddressfromlatlong(lati, longi, ctx))
            )
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
            val cameraPosition =
                CameraPosition.Builder().target(LatLng(lati, longi)).zoom(15f).tilt(30f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getAddressfromlatlong(
        lati: Double,
        longi: Double,
        ctx: Context?
    ): String? {
        var res: String? = ""
        if (lati != 0.0) {
            val geocoder = Geocoder(ctx, Locale.getDefault())
            try {
                val addresses =
                    geocoder.getFromLocation(lati, longi, 1)
                val address = addresses[0].subLocality
                val cityName = addresses[0].locality
                val stateName = addresses[0].adminArea
                res = address
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return res
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MAP_ACTIVITY && resultCode == Activity.RESULT_OK) {
            lati = data!!.getDoubleExtra("lati", 0.0)
            longi = data.getDoubleExtra("longi", 0.0)
            setMarker(lati, longi)
        }
    }

    private fun setMarker(re_lati: Double, re_longi: Double) {
        mMap!!.clear()
        val sydney = LatLng(re_lati, re_longi)
        mMap!!.addMarker(
            MarkerOptions().position(sydney).title(getAddressfromlatlong(re_lati, re_longi, ctx))
        )
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        val cameraPosition =
            CameraPosition.Builder().target(LatLng(re_lati, re_longi)).zoom(15f).tilt(30f).build()
        mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }


}
