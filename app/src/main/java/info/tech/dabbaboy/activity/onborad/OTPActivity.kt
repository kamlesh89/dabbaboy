package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_otp.*
import org.json.JSONObject


class OTPActivity : BaseActivity() {

    lateinit var ctx: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_otp
    }


    private fun initXml() {
        ctx = this
        et_otp_otp.addTextChangedListener(textWatcher)
        tv_otp_sendagain.setOnClickListener(View.OnClickListener {
            resendOTP(ApiObjects.getforgotRequest(intent.getStringExtra("mobile")))
        })
    }

    private fun resendOTP(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_FORGOT)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    var info_obj = response.getJSONObject("info")
                    var status = info_obj.getInt("status")
                    var message = info_obj.getString("message")
                    Utility.snacbarShow(message, coordinator_otp)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
            if (start == 5) {
                if (ConnectionDetector.isConnected()) {
                    Utility.hideKeyboard(this@OTPActivity)
                    submitOTP(
                        ApiObjects.getVerifyOTP(
                            intent.getStringExtra("mobile"),
                            et_otp_otp.text.toString()
                        ), getApi()
                    )
                } else {
                    Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_otp)
                }
            }
        }

        override fun afterTextChanged(s: Editable) {

        }
    }

    fun getApi(): String {
        if (intent.getStringExtra("type").equals(Constant.OTPACTIVITY_FORGOT))
            return WebApis.API_FORGOT
        else
            return WebApis.API_LOGIN
    }

    private fun submitOTP(myObject: JSONObject, api: String) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(api)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            if (getApi().equals(WebApis.API_LOGIN)) {
                var data_obj = response.getJSONObject("data")
                SessionManager(this).createLoginSession(
                    data_obj.getString("id"), data_obj.getString("name")
                    , data_obj.getString("email"), data_obj.getString("mobile_no")
                    , data_obj.getString("address"), data_obj.getString("pincode")
                )
                finishAffinity()
                startActivity(Intent(ctx, RegisterSuccessActivity::class.java))
            } else {
                startActivity(
                    Intent(ctx, CreatePasswordActivity::class.java)
                        .putExtra("mobile", intent.getStringExtra("mobile"))
                )
                finish()
            }
        } else {
            et_otp_otp.setText("")
            Utility.snacbarShow(message, coordinator_otp)
        }
    }

}

