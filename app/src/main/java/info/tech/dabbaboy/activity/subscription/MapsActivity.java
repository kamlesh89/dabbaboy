package info.tech.dabbaboy.activity.subscription;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import info.tech.dabbaboy.R;
import info.tech.dabbaboy.util.LocationTrack;
import info.tech.dabbaboy.util.Utility;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, View.OnClickListener {

    Context ctx;
    private GoogleMap mMap;
    Button submit_bt;
    ImageView current_iv;

    LocationTrack locationTrack;
    double lati = 0, longi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ctx = this;
        submit_bt = findViewById(R.id.bt_map_submit);
        current_iv = findViewById(R.id.iv_map_current);

        lati = getIntent().getDoubleExtra("lati", 0);
        longi = getIntent().getDoubleExtra("longi", 0);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationTrack = new LocationTrack(ctx);

        submit_bt.setOnClickListener(this);
        current_iv.setOnClickListener(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        setMarker(true);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

//                setMarker(latLng.latitude, latLng.longitude, false);
            }
        });
        mMap.setOnInfoWindowClickListener(this);

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                double latitude = marker.getPosition().latitude;
                double longitude = marker.getPosition().longitude;
                Utility.toastView(latitude + "", ctx);
            }
        });
    }

    private void setMarker(boolean zoom) {
        locationTrack = new LocationTrack(ctx);

        if (lati == 0) {
            lati = locationTrack.getLatitude();
            longi = locationTrack.getLongitude();
        }
        mMap.clear();
        LatLng sydney = new LatLng(lati, longi);
//        mMap.addMarker(new MarkerOptions().position(sydney).title(Utility.getAddress(latitude, longitude, ctx))).setDraggable(false);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (zoom) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lati, longi)).zoom(16).tilt(30).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
//        Utility.toastView(ctx, "maker info");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_map_submit:
                LatLng center = mMap.getCameraPosition().target;
                double la = center.latitude;
                double lo = center.longitude;

                Intent returnIntent = new Intent();
                returnIntent.putExtra("lati", la);
                returnIntent.putExtra("longi", lo);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                break;

            case R.id.iv_map_current:
                setMarker(true);
                break;
        }
    }
}
