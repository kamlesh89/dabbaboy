package info.tech.dabbaboy.activity.subscription;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.tech.dabbaboy.R;
import info.tech.dabbaboy.adapter.MealAdapter;
import info.tech.dabbaboy.pojo.MyMeal;
import info.tech.dabbaboy.util.ApiObjects;
import info.tech.dabbaboy.util.ConnectionDetector;
import info.tech.dabbaboy.util.JsonParser;
import info.tech.dabbaboy.util.Utility;
import info.tech.dabbaboy.util.WebApis;

public class PackagesActivity extends AppCompatActivity {

    Context ctx;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        initXml();
        if (ConnectionDetector.isConnected()) {
            loadPackages(ApiObjects.getMode("0"));
        } else {
            CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinator_package);
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout);
        }
    }

    private void initXml() {
        ctx = this;
        recyclerView = findViewById(R.id.rv_subscription_recyclerView);
        TextView head_tv = findViewById(R.id.tv_header_title);
        ImageView back_iv = findViewById(R.id.iv_header_back);
        head_tv.setText(ctx.getString(R.string.title_package));
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void loadPackages(JSONObject mode) {

        Utility.showLoader(ctx);
        AndroidNetworking.post(WebApis.API_PACKAGE)
                .addJSONObjectBody(mode)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utility.hideLoader();
                        setResponse(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Utility.hideLoader();
                    }
                });
    }

    private void setResponse(JSONObject response) {

        try {
            JSONObject info_obj = response.getJSONObject("info");
            int status = info_obj.getInt("status");
            String message = info_obj.getString("message");
            if (status == 200) {
                JSONArray data_array = response.getJSONArray("data");
                List<MyMeal> list = JsonParser.getPackagelist(data_array);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(new MealAdapter(list, ctx));
            } else {
                CoordinatorLayout coordinatorLayout = findViewById(R.id.coordinator_package);
                Utility.snacbarShow(message, coordinatorLayout);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
