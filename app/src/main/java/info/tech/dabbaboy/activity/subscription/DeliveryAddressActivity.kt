package info.tech.dabbaboy.activity.subscription

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.pojo.ActivePackage
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_delivery_address.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONException
import org.json.JSONObject
import java.lang.StringBuilder
import kotlin.collections.ArrayList


class DeliveryAddressActivity : AppCompatActivity(), View.OnClickListener {

    var sessionManager: SessionManager? = null
    lateinit var ctx: Context
    lateinit var active_packs: ArrayList<ActivePackage>

    var lunch_id = ""
    var dinner_id = ""

    val TYPE_LUNCH = 100;
    val TYPE_DINNER = 101;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_address)
        initXml()
    }

    private fun initXml() {
        ctx = this

        tv_header_title.setText("Delivery Address")
        iv_header_back.setOnClickListener(this)
        iv_delivaddress_dinneredit.setOnClickListener(this)
        iv_delivaddress_lunchedit.setOnClickListener(this)

        sessionManager = SessionManager(ctx)
        if (ConnectionDetector.isConnected()) {
            getUserDetails(ApiObjects.getUserdetails(sessionManager!!.getData(SessionManager.KEY_ID)))
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_deliveryaddress)
        }
    }

    private fun getUserDetails(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_USER_ADDRESS_DETAILS)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                sv_delivaddress_scrollview.visibility = View.VISIBLE
                val data_array = response.getJSONArray("data")
                active_packs = JsonParser.getActivePackage(data_array)
                setActiveSpinner()
            } else {
                tv_delivaddress_notavail.visibility = View.VISIBLE
                Utility.snacbarShow(message, coordinator_deliveryaddress)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setActiveSpinner() {
        var list = java.util.ArrayList<String>()
        for (i in 0..active_packs!!.size - 1) {
            var no = i + 1
            list.add("Package $no ")
        }
        sp_delivaddress_activepack.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list)

        sp_delivaddress_activepack.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    setData(active_packs[position])
                }
            }
    }

    private fun setData(activePackage: ActivePackage) {

        var lunch = activePackage.orderdetail.getString("lunch")
        var dinner = activePackage.orderdetail.getString("dinner")

        if (lunch.equals("no")) {
            ll_delivaddress_lunchaddress.visibility = View.GONE
        } else {
            ll_delivaddress_lunchaddress.visibility = View.VISIBLE
            lunch_id = activePackage.id
            tv_delivaddress_lunchaddress.setText(getAddress(activePackage.lunch_address))
        }
        if (dinner.equals("no")) {
            ll_delivaddress_dinneraddress.visibility = View.GONE
        } else {
            dinner_id = activePackage.id
            ll_delivaddress_dinneraddress.visibility = View.VISIBLE
            tv_delivaddress_dinneraddress.setText(getAddress(activePackage.dinner_address))
        }
    }

    private fun getAddress(lunchAddress: JSONObject?): String {
        var builder = StringBuilder()
        builder.append(lunchAddress?.getString("building_name") + " ")
        builder.append(lunchAddress?.getString("street") + " ")
        builder.append(lunchAddress?.getString("landmark") + " ")
        builder.append(lunchAddress?.getString("area"))
        return builder.toString()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.iv_header_back -> {
                finishAffinity()
                startActivity(Intent(ctx, MainActivity::class.java))
            }
            R.id.iv_delivaddress_lunchedit -> {
                startActivityForResult(Intent(ctx, AddressActivity::class.java), TYPE_LUNCH)
            }
            R.id.iv_delivaddress_dinneredit -> {
                startActivityForResult(Intent(ctx, AddressActivity::class.java), TYPE_DINNER)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == TYPE_LUNCH && resultCode == Activity.RESULT_OK) {
            var address = data?.getStringExtra("address")
            tv_delivaddress_lunchaddress.setText(address)
            var address_id = data?.getStringExtra("id")
            update(
                "Lunch",
                active_packs[sp_delivaddress_activepack.selectedItemPosition].lunch_address_id,
                address_id!!
            )
        } else if (requestCode == TYPE_DINNER && resultCode == Activity.RESULT_OK) {
            var address = data?.getStringExtra("address")
            tv_delivaddress_dinneraddress.setText(address)
            var address_id = data?.getStringExtra("id")
            update(
                "Dinner",
                active_packs[sp_delivaddress_activepack.selectedItemPosition].dinner_address_id,
                address_id!!
            )
        }
    }

    private fun update(type: String, address_id: String, update_address_id: String) {
        if (ConnectionDetector.isConnected()) {
            updateAddress(
                ApiObjects.updateAddressid(
                    active_packs[sp_delivaddress_activepack.selectedItemPosition].id, type
                    , address_id, update_address_id
                )
            )
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_deliveryaddress)
        }
    }

    // update address
    private fun updateAddress(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_ORDER_ADDRESS)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setAddressResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setAddressResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            Utility.snacbarShow(message, coordinator_deliveryaddress)
            initXml()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}
