package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.util.SessionManager
import kotlinx.android.synthetic.main.activity_register_success.*

class RegisterSuccessActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        bt_success_claimnow.setOnClickListener(this)
        bt_success_claimlater.setOnClickListener(this)
        sessionManager = SessionManager(ctx)

        tv_success_name.setText(sessionManager.getData(SessionManager.KEY_NAME))
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_register_success
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_success_claimnow -> {
                finishAffinity()
                startActivity(
                    Intent(ctx, MainActivity::class.java)
                        .putExtra("type", "free")
                )
            }
            R.id.bt_success_claimlater -> {
                finishAffinity()
                startActivity(
                    Intent(ctx, MainActivity::class.java)
                        .putExtra("type", "noti")
                )
            }
        }
    }
}
