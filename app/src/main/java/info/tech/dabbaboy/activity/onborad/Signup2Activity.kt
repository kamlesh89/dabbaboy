package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_signup2.*
import org.json.JSONObject

class Signup2Activity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        bt_signup2_register.setOnClickListener(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_signup2
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_signup2_register -> {
                if (isValid()) {
                    userRegister(
                        ApiObjects.getUserRegister(
                            intent.getStringExtra("name"),
                            met_signup2_mobile.text.toString(),
                            intent.getStringExtra("email"),
                            sessionManager.getData(SessionManager.KEY_AREA),
                            "Indore", "M.P.", sessionManager.getData(SessionManager.KEY_PINCODE),
                            met_signup2_password.text.toString()
                        )
                    )
                }
            }
        }
    }


    private fun userRegister(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_LOGIN)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            startActivity(
                Intent(ctx, OTPActivity::class.java)
                    .putExtra("type", Constant.OTPACTIVITY_LOGIN)
                    .putExtra("mobile", met_signup2_mobile.text.toString())
            )
        } else {
            Utility.snacbarShow(message, coordinator_signup2)
        }
    }


    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_signup2)
            return false
        } else if (met_signup2_mobile.text.toString().length != 10) {
            met_signup2_mobile.setError("Enter 10 Digit Mobile Number")
            met_signup2_mobile.requestFocus()
            return false
        } else if (met_signup2_password.text.toString().isEmpty()) {
            met_signup2_password.setError("Enter Password")
            met_signup2_password.requestFocus()
            return false
        } else if (met_signup2_confpass.text.toString().isEmpty()) {
            met_signup2_confpass.setError("Enter Confirm Password")
            met_signup2_confpass.requestFocus()
            return false
        } else if (!met_signup2_confpass.text.toString()
                .equals(met_signup2_password.text.toString())
        ) {
            met_signup2_confpass.setError("Password not matched")
            met_signup2_confpass.requestFocus()
            return false
        }
        return valid
    }

}
