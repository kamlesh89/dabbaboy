package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
        checkLogin();
    }

    private fun checkLogin() {
        if (SessionManager(this).isLoggedIn) {
            startActivity(Intent(ctx, MainActivity::class.java))
            finish()
        }
    }

    private fun initXml() {
        ctx = this
        bt_login_signin.setOnClickListener(this)
        tv_login_forgot.setOnClickListener(this)
        tv_login_signup.setOnClickListener(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.bt_login_signin -> {
                if (isValid()) {
                    userLogin(
                        ApiObjects.getUserLogin(
                            met_login_mobile.text.toString(),
                            met_login_pass.text.toString()
                        )
                    )
                }
            }
            R.id.tv_login_forgot -> {
                startActivity(Intent(ctx, ForgotActivity::class.java))
            }
            R.id.tv_login_signup -> {
                startActivity(Intent(ctx, SelectCityActivity::class.java))
            }
        }
    }

    private fun userLogin(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_LOGIN)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var data_obj = response.getJSONObject("data")
            SessionManager(this).createLoginSession(
                data_obj.getString("id"), data_obj.getString("name")
                , data_obj.getString("email"), data_obj.getString("mobile_no")
                , data_obj.getString("address"), data_obj.getString("pincode")
            )
            finishAffinity()
            startActivity(Intent(ctx, MainActivity::class.java))
        } else {
            Utility.snacbarShow(message, coordinator_login)
        }
    }


    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_login)
            return false
        } else if (met_login_mobile.text.toString().length != 10) {
            met_login_mobile.setError("Enter 10 Digit Mobile Number")
            met_login_mobile.requestFocus()
            return false
        } else if (met_login_pass.text.toString().isEmpty()) {
            met_login_pass.setError("Enter Password")
            met_login_pass.requestFocus()
            return false
        }
        return valid
    }

}
