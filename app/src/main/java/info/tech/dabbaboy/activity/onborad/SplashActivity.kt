package info.tech.dabbaboy.activity.onborad

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.Constant
import info.tech.dabbaboy.util.LocationTrack
import info.tech.dabbaboy.util.Utility
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startProgress()
        Handler().postDelayed(Runnable {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, Constant.SPLASH_TIMEOUT)
    }

    private fun startProgress() {
        var progressStatus = 0;
        Thread(Runnable {
            run {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    Thread.sleep(50);
                    pb_splash_progressBar.setProgress(progressStatus)
                }
            }
        }).start()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }
}
