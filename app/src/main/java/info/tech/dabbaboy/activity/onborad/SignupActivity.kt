package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.ConnectionDetector
import info.tech.dabbaboy.util.Utility
import kotlinx.android.synthetic.main.activity_signup.*
import org.json.JSONException
import java.util.*

class SignupActivity : BaseActivity(), View.OnClickListener, OnConnectionFailedListener {

    lateinit var ctx: Context

    //    facebook login
    var facebbokloginButton: LoginButton? = null
    var callbackManager: CallbackManager? = null
    private val EMAIL = "email"

    // google login
    private var mGoogleApiClient: GoogleApiClient? = null
    var mGoogleSignInClient: GoogleSignInClient? = null
    val RC_SIGN_IN = 12345

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        bt_signup_signup.setOnClickListener(this)
        ll_signup_facebook.setOnClickListener(this)
        ll_signup_google.setOnClickListener(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_signup
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_signup_signup -> {
                if (isValid()) {
                    if (isAcceptedpolicy()) {
                        var name = met_signup_username.text.toString();
                        var email = met_signup_username.text.toString();
                        startNext(name, email)
                    }
                }
            }
            R.id.ll_signup_facebook -> {
                if (isAcceptedpolicy()) {
                    initSocialFacebook()
                    faceBookLogin()
                }
            }
            R.id.ll_signup_google -> {
                if (isAcceptedpolicy()) {
                    initSocialGoogle()
                    signIn()
                }
            }
        }
    }

    public fun startNext(name: String?, email: String?) {
        startActivity(
            Intent(ctx, Signup2Activity::class.java)
                .putExtra("name", name)
                .putExtra("email", email)
        )
    }

    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_signup)
            return false
        } else if (met_signup_username.text.toString().isEmpty()) {
            met_signup_username.setError("Enter your name")
            met_signup_username.requestFocus()
            return false
        } else if (!Utility.isEmailValid(met_signup_email.text.toString())) {
            met_signup_email.setError("Enter Valid Email Address")
            met_signup_email.requestFocus()
            return false
        } else if (sw_signup_terms.isActivated) {
            Utility.snacbarShow("", coordinator_signup)
            return false
        }
        return valid
    }


    // google
    public fun initSocialGoogle(): Unit {
        // google
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(ctx)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun signIn() {
        signOut()
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun signOut() {
        if (mGoogleApiClient!!.isConnected) Auth.GoogleSignInApi.signOut(mGoogleApiClient)
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        val res = result.toString()
        if (result.isSuccess) {
            val acct = result.signInAccount
            val personName = acct!!.displayName
            val email = acct.email
            val id = acct.id
            startNext(personName, email)
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }

    // facebook login
    public fun initSocialFacebook(): Unit {
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut()
        }
        callbackManager = CallbackManager.Factory.create()
        facebbokloginButton = findViewById(R.id.login_button)
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    // App code
                    Utility.toastView("login-success", ctx)
                }

                override fun onCancel() {
                    // App code
                    Utility.toastView("login-cancel", ctx)
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Utility.toastView("login-error", ctx)
                }
            })
    }

    private fun faceBookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
            this,
            Arrays.asList(
                "public_profile",
                EMAIL
            )
        )
        callbackManager = CallbackManager.Factory.create()
        facebbokloginButton = findViewById(R.id.login_button)
        login_button.setReadPermissions(Arrays.asList<String>(EMAIL))
        login_button.setReadPermissions("public_profile")
        login_button.registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    // Facebook Email address
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, response ->
                        Log.v("LoginActivity Response ", response.toString())
                        try {
                            val id = `object`.getString("id")
                            val Name = `object`.getString("name")
                            val email = `object`.getString("email")
                            startNext(Name, email)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email,gender, birthday")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    // App code
                    Utility.toastView("cancel", ctx)
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Utility.toastView(exception.toString(), ctx)
                }
            })
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result =
                Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        } else {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun isAcceptedpolicy(): Boolean {
        if (sw_signup_terms.isChecked)
            return true
        else
            Utility.snacbarShow("Please Accept Terms and Policy", coordinator_signup)
        return false
    }

}
