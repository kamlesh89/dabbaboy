package info.tech.dabbaboy.activity

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.subscription.DeliveryAddressActivity
import info.tech.dabbaboy.activity.subscription.PackagesActivity
import info.tech.dabbaboy.fragment.*
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_packagedetails.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.layout_navigation.*
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var ctx: Context
    var sessionManager: SessionManager? = null
    var firsttime = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initXml()
        if (ConnectionDetector.isConnected()) {
            getUserProfile(ApiObjects.getUserData(sessionManager?.getData(SessionManager.KEY_ID)))
        }
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        ll_nav_home.setOnClickListener(this)
        ll_nav_package.setOnClickListener(this)
        ll_nav_delivery.setOnClickListener(this)
        ll_nav_wallet.setOnClickListener(this)
        ll_nav_free.setOnClickListener(this)
        ll_nav_orderhistory.setOnClickListener(this)
        ll_nav_changearea.setOnClickListener(this)
        ll_nav_contactus.setOnClickListener(this)
        ll_nav_aboutus.setOnClickListener(this)

        tv_nav_name.setText(sessionManager?.getData(SessionManager.KEY_NAME))
        tv_nav_mobile.setText(sessionManager?.getData(SessionManager.KEY_MOBILE))

        iv_main_menu.setOnClickListener(View.OnClickListener {
            opencloseDrawer()
        })

        // navigation click
        ll_nav_logout.setOnClickListener(this)
    }

    private fun getUserProfile(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_USER_PROFILE)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val user_object = response.getJSONObject("data")
                var usertype = user_object.getString("subscribe_status")
                sessionManager?.setData(SessionManager.KEY_USER_SUBSCRIBE, usertype)

                setHome()
            } else {
                Utility.snacbarShow(message, coordinator_packagedetail)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }


    private fun setHome() {

        if (intent.extras != null && firsttime == 0) {
            var act_type = intent.getStringExtra("type")
            if (act_type.equals("free")) {
                Utility.setFragment(
                    FreeDabbaFragment(ctx, ctx.getString(R.string.title_freedabba)),
                    ctx.getString(R.string.title_freedabba),
                    this
                )
            } else if (act_type.equals("noti")) {
                Utility.setFragment(
                    NotificationFragment(ctx, ctx.getString(R.string.title_noti)),
                    ctx.getString(R.string.title_noti),
                    this
                )
            }
        } else {
            if (sessionManager?.getData(SessionManager.KEY_USER_SUBSCRIBE).equals("0")) {
                Utility.setFragment(
                    HomeUSFragment(ctx, ctx.getString(R.string.title_homeus)),
                    "Home",
                    this
                )
            } else {
                Utility.setFragment(
                    HomeSFragment(ctx, ctx.getString(R.string.title_homeus)),
                    "Home",
                    this
                )
            }
        }
        firsttime = 1
    }

    override fun onClick(p0: View?) {
        opencloseDrawer()
        when (p0?.id) {
            R.id.ll_nav_home -> {
                setHome()
            }
            R.id.ll_nav_package -> {
                startActivity(Intent(ctx, PackagesActivity::class.java))
            }
            R.id.ll_nav_delivery -> {
                startActivity(Intent(ctx, DeliveryAddressActivity::class.java))
            }
            R.id.ll_nav_wallet -> {
                Utility.setFragment(
                    WalletFragment(ctx, ctx.getString(R.string.title_wallet)),
                    ctx.getString(R.string.title_wallet),
                    this
                )
            }
            R.id.ll_nav_free -> {
                Utility.setFragment(
                    FreeDabbaFragment(ctx, ctx.getString(R.string.title_freedabba)),
                    ctx.getString(R.string.title_freedabba),
                    this
                )
            }
            R.id.ll_nav_orderhistory -> {
                Utility.setFragment(
                    OrderHistoryFragment(ctx, ctx.getString(R.string.title_order)),
                    ctx.getString(R.string.title_order),
                    this
                )
            }
            R.id.ll_nav_changearea -> {
            }
            R.id.ll_nav_aboutus -> {
                Utility.setFragment(
                    AboutusFragment(ctx, ctx.getString(R.string.title_aboutus)),
                    ctx.getString(R.string.title_aboutus),
                    this
                )
            }
            R.id.ll_nav_contactus -> {
                Utility.setFragment(
                    ContactUsFragment(ctx, ctx.getString(R.string.title_contactus)),
                    ctx.getString(R.string.title_contactus),
                    this
                )
            }
            R.id.ll_nav_logout -> {
                userLogout()
            }
        }
    }

    private fun opencloseDrawer() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            drawer.openDrawer(GravityCompat.START)
        }
    }

    fun setFrag(frag: Fragment, tag: String) {
        Utility.setFragment(frag, tag, this)
    }

    private fun userLogout() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(ctx.getString(R.string.logout_msg))
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
                SessionManager(ctx).logoutUser(this)
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                dialog.dismiss()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    fun setHeadTitle(title: String) {
        tv_main_title.setText(title)
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (tv_main_title.text.toString() == ctx.getString(R.string.title_homeus)) {
                closeApp()
            } else {
                setHome()
            }
        }
    }

    private fun closeApp() {
        val builder1 = AlertDialog.Builder(ctx)
        builder1.setMessage("Are you sure , you want to close the application ?")
        builder1.setCancelable(true)
        builder1.setPositiveButton(
            "Yes"
        ) { dialog, id ->
            finishAffinity()
            dialog.cancel()
        }
        builder1.setNegativeButton(
            "No"
        ) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }


}
