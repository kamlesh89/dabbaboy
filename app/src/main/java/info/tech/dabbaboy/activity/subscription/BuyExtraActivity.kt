package info.tech.dabbaboy.activity.subscription

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.tabs.TabLayout
import info.tech.dabbaboy.R
import info.tech.dabbaboy.fragment.BuyExtraFragment
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_buy_extra.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONException
import org.json.JSONObject


class BuyExtraActivity : AppCompatActivity() {

    lateinit var ctx: Context
    lateinit var date_list_from_today: ArrayList<OrderDates>
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_extra)
        initXml()
        getdata(sessionManager.getIntData(SessionManager.KEY_EXTRA_POSISTION))
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)
        tv_header_title.setText("Buy Extra Tiffin")
        iv_header_back.setOnClickListener(View.OnClickListener { finish() })
    }

    fun getdata(i: Int) {
        if (ConnectionDetector.isConnected()) {
            getOrderDates(ApiObjects.getOrderdates(intent.getStringExtra("id")), i)
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_extra)
        }
    }

    private fun getOrderDates(apibody: JSONObject, position: Int) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ORDER_DATES)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response, position)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject, position: Int) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val user_object = response.getJSONObject("data")
                var date_data = user_object.getJSONArray("user_order")
                if (date_data.length() > 0) {
                    date_list_from_today = JsonParser.getOrderDatelistfromToday(date_data)
                    setPagination(date_list_from_today, position)
                }
            } else {
                Utility.snacbarShow(message, coordinator_extra)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setPagination(
        filterDateList: ArrayList<OrderDates>,
        position: Int
    ) {
        var tabLayout = findViewById<TabLayout>(R.id.tl_extra_tabLayout)
        var viewPager = findViewById<ViewPager>(R.id.vp_extra_viewPager)
        if (tabLayout.tabCount > 0) {
            tabLayout.removeAllTabs()
        }

        for (i in 0..filterDateList.size - 1) {
            tabLayout!!.addTab(
                tabLayout!!.newTab().setText(Utility.changeDateFormat(filterDateList[i].date))
            )
        }
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout!!.tabMode = TabLayout.MODE_SCROLLABLE

/*
        val adapter =
            AdapterScheduling(this, supportFragmentManager, tabLayout!!.tabCount, filterDateList)
        viewPager!!.adapter = adapter
        viewPager!!.offscreenPageLimit = 0
        viewPager!!.setCurrentItem(position)

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
*/

        Utility.setExtraFragment(
            BuyExtraFragment(ctx, filterDateList[0], 0),
            "", this@BuyExtraActivity
        )

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
//                viewPager!!.currentItem = tab.position
                Utility.setExtraFragment(
                    BuyExtraFragment(ctx, filterDateList[tab.position], tab.position),
                    "", this@BuyExtraActivity
                )
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    fun setRefresh(position: Int) {
        sessionManager?.setIntData(SessionManager.KEY_EXTRA_POSISTION, position)
        finish()
        startActivity(intent.putExtra("pos", position))
    }

    fun getLastOrder(): OrderDates {
        return date_list_from_today[date_list_from_today.size - 1]
    }
}
