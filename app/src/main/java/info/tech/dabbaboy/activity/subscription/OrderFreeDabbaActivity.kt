package info.tech.dabbaboy.activity.subscription

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_order_free_dabba.*
import kotlinx.android.synthetic.main.activity_packagedetails.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONException
import org.json.JSONObject

class OrderFreeDabbaActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener,
    View.OnClickListener {

    lateinit var ctx: Context
    var lunch_list: ArrayList<String>? = null
    var dinner_list: ArrayList<String>? = null
    var sessionManager: SessionManager? = null

    val INTENT_SELECT_ADDRESS = 123
    var address_id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_free_dabba)
        initXml()
    }

    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)

        rb_freedabba_lunch.setOnCheckedChangeListener(this)
        rb_freedabba_dinner.setOnCheckedChangeListener(this)

        iv_header_back.setOnClickListener(this)
        tv_freedabba_address.setOnClickListener(this)
        tv_freedabba_date.setOnClickListener(this)
        bt_freedabba_submit.setOnClickListener(this)
        tv_header_title.setText("Order Free Dabba")

        if (ConnectionDetector.isConnected()) {
            loadTimimg(ApiObjects.getMode("0"))
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator)
        }
    }

    private fun loadTimimg(mode: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_SERVICE_TIME)
            .addJSONObjectBody(mode)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setTimeResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setTimeResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            if (status == 200) {
                val data_obj = response.getJSONObject("data")
                var lunch_array = data_obj.getJSONArray("Lunch")
                var dinner_array = data_obj.getJSONArray("Dinner")
                lunch_list = JsonParser.gettimelist(lunch_array)
                dinner_list = JsonParser.gettimelist(dinner_array)
                setTimspinner(lunch_list!!)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setTimspinner(list: ArrayList<String>) {
        sp_freedabba_time.setAdapter(
            ArrayAdapter(
                ctx,
                android.R.layout.simple_spinner_dropdown_item,
                list
            )
        )
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (rb_freedabba_lunch.isChecked) {
            setTimspinner(lunch_list!!)
        } else {
            setTimspinner(dinner_list!!)
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_freedabba_submit -> {
                if (isValid()) {
                    var body = ApiObjects.getSubmitfreeOrder(
                        sessionManager!!.getData(SessionManager.KEY_ID),
                        tv_freedabba_date.text.toString(),
                        sp_freedabba_time.selectedItem.toString(),
                        getType(), address_id!!
                    )
                    submitOrder(body);
                }
            }
            R.id.tv_freedabba_date -> {
                showCalendar()
            }
            R.id.tv_freedabba_address -> {
                startActivityForResult(
                    Intent(ctx, AddressActivity::class.java),
                    INTENT_SELECT_ADDRESS
                )
            }

            R.id.iv_header_back -> {
                finish()
            }
        }
    }

    private fun getType(): String {
        if (rb_freedabba_lunch.isChecked) {
            return "Lunch"
        } else {
            return "Dinner"
        }
    }

    private fun showCalendar() {
        val date: String = Utility.getDate()
        val mDay = date.substring(8, 10).toInt()
        val mMonth = date.substring(5, 7).toInt() - 1
        val mYear = date.substring(0, 4).toInt()
        val datePickerDialog = DatePickerDialog(
            ctx,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var monthOfYear = monthOfYear
                monthOfYear = monthOfYear + 1
                var day1 = dayOfMonth.toString()
                var month1 = monthOfYear.toString()
                if (day1.length == 1) {
                    day1 = "0$day1"
                }
                if (month1.length == 1) {
                    month1 = "0$month1"
                }
                tv_freedabba_date.setText("$day1-$month1-$year")
            }, mYear, mMonth, mDay
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INTENT_SELECT_ADDRESS && resultCode == Activity.RESULT_OK) {
            address_id = data?.getStringExtra("id")
            tv_freedabba_address.setText(data?.getStringExtra("address"))
        }
    }

    private fun isValid(): Boolean {
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator)
            return false
        } else if (tv_freedabba_date.text.toString().isEmpty()) {
            Utility.snacbarShow("Please select date", coordinator)
            return false
        } else if (tv_freedabba_address.text.toString().isEmpty()) {
            Utility.snacbarShow("Please select Address", coordinator)
            return false
        } else if (sp_freedabba_time.selectedItemPosition == 0) {
            Utility.snacbarShow("Please select Delivery Time", coordinator)
            return false
        }
        return true
    }

    // submit order
    private fun submitOrder(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ORDER_FREE_DABBA)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            Utility.snacbarShow(message, coordinator)
            if (status == 200) {
                finishAffinity()
                startActivity(Intent(ctx, MainActivity::class.java))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

}
