package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_select_city.*
import org.json.JSONObject


class SelectCityActivity : BaseActivity() {

    lateinit var ctx: Context
    lateinit var list: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        et_selectcity_pincode.addTextChangedListener(textWatcher)
        bt_selectcity_procced.setOnClickListener(View.OnClickListener {
            if (isValid()) {
                SessionManager(ctx).setData(
                    SessionManager.KEY_PINCODE,
                    et_selectcity_pincode.text.toString()
                )
                SessionManager(ctx).setData(
                    SessionManager.KEY_AREA,
                    sp_selectcity_area.selectedItem.toString()
                )
                startActivity(Intent(ctx, SignupActivity::class.java))
            }
        })
    }

    var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
            if (et_selectcity_pincode.text.toString().length == 6) {
                if (ConnectionDetector.isConnected()) {
                    Utility.hideKeyboard(this@SelectCityActivity)
                    submitPincode(
                        ApiObjects.getArea(
                            et_selectcity_pincode.text.toString()
                        )
                    )
                } else {
                    Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_selectcity)
                }
            } else {
                list = ArrayList<String>()
                setSpinner()
            }
        }
    }


    private fun setSpinner() {

        var area_adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_dropdown_item_1line, list);
        sp_selectcity_area.adapter = area_adapter
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_select_city
    }

    fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_selectcity)
            return false
        } else if (et_selectcity_pincode.text.toString().length != 6) {
            et_selectcity_pincode.setError("Enter Valid Pincode")
            et_selectcity_pincode.requestFocus()
            return false
        } else if (list.size == 0) {
            Utility.snacbarShow("Select Area", coordinator_selectcity)
            return false
        }
        return valid
    }

    private fun submitPincode(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_AREA)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var data_array = response.getJSONArray("data")
            list = JsonParser.getAreaList(data_array)
            setSpinner()
        } else {
            et_selectcity_pincode.setError("we are not serving in this area")
            Utility.snacbarShow(message, coordinator_selectcity)
        }
    }

}
