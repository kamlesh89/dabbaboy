package info.tech.dabbaboy.activity.onborad

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_create_pass.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject

class CreatePasswordActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
    }

    private fun initXml() {
        ctx = this
        bt_createpass_submit.setOnClickListener(this)
        iv_header_back.setOnClickListener(this)
        tv_header_title.setText(ctx.getString(R.string.title_create))
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_create_pass
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.bt_createpass_submit -> {
                if (isValid()) {
                    createMypassword(
                        ApiObjects.getforgotcreatePass(
                            intent.getStringExtra("mobile"),
                            met_createpass_new.text.toString()
                        )
                    )
                }
            }
            R.id.iv_header_back -> {
                finish()
            }
        }
    }

    private fun createMypassword(myObject: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_FORGOT)
            .addJSONObjectBody(myObject)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            finishAffinity()
            startActivity(Intent(ctx, LoginActivity::class.java))
        } else {
            Utility.snacbarShow(message, coordinator_createpass)
        }
    }


    private fun isValid(): Boolean {
        var valid = true
        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_createpass)
            return false
        } else if (met_createpass_new.text.toString().isEmpty()) {
            met_createpass_new.setError("Enter New Password")
            met_createpass_new.requestFocus()
            return false
        } else if (met_createpass_newconf.text.toString().isEmpty()) {
            met_createpass_newconf.setError("Enter Confirm Password")
            met_createpass_newconf.requestFocus()
            return false
        } else if (!met_createpass_newconf.text.toString()
                .equals(met_createpass_new.text.toString())
        ) {
            met_createpass_newconf.setError("Password not matched")
            met_createpass_newconf.requestFocus()
            return false
        }
        return valid
    }

}
