package info.tech.dabbaboy.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import info.tech.dabbaboy.BaseActivity
import info.tech.dabbaboy.R
import info.tech.dabbaboy.adapter.AdapterAddress
import info.tech.dabbaboy.pojo.Address
import info.tech.dabbaboy.util.*
import info.tech.dabbaboy.util.Utility.Companion.hideLoader
import info.tech.dabbaboy.util.Utility.Companion.showLoader
import info.tech.dabbaboy.util.Utility.Companion.snacbarShow
import kotlinx.android.synthetic.main.activity_address.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONException
import org.json.JSONObject


class AddressActivity : BaseActivity(), View.OnClickListener {

    lateinit var ctx: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initXml()
        getdata()
    }

    private fun getdata() {
        if (ConnectionDetector.isConnected()) {
            loadAddress(ApiObjects.getAlladdress(SessionManager(ctx).getData(SessionManager.KEY_ID)))
        } else {
            snacbarShow(
                ctx.getString(R.string.no_internet),
                coordinator_address
            )
        }
    }

    private fun loadAddress(apibody: JSONObject) {

        showLoader(ctx)
        AndroidNetworking.post(WebApis.API_GET_ADDRESS)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val data_array = response.getJSONArray("data")
                var list = JsonParser.getAddressList(data_array)
                setAdapter(list)
            } else {
                snacbarShow(message, coordinator_address)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_address
    }


    private fun setAdapter(list: ArrayList<Address>) {
        rv_address_recycler.setHasFixedSize(true)
        rv_address_recycler.setLayoutManager(LinearLayoutManager(this))
        rv_address_recycler.adapter = AdapterAddress(ctx, list)
    }

    fun returnselectedAddress(address: Address) {
        var add = "${address.house_no} , ${address.street},${address.landmark},${address.area}"
        setResult(
            Activity.RESULT_OK, Intent().putExtra("address", add)
                .putExtra("id", address.id)
        )
        finish()
    }

    private fun initXml() {
        ctx = this
        tv_header_title.setText(ctx.getString(R.string.title_address))
        iv_header_back.setOnClickListener(this)
        bt_address_addnew.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {

        when (p0?.id) {
            R.id.iv_header_back -> {
                finish()
            }
            R.id.bt_address_addnew -> {
                requestPermission()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.ADDRESS_INTENT_CODE && resultCode == Activity.RESULT_OK) {
            getdata()
        }
    }

    // location permission
    fun requestPermission(): Unit {
        Dexter.withActivity(this).withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        startActivityForResult(
                            Intent(ctx, AddAddressActivity::class.java)
                                .putExtra("type", "add"),
                            Constant.ADDRESS_INTENT_CODE
                        )
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Utility.toastView("Error occurred! ", ctx)
            }
            .onSameThread()
            .check()
    }


}
