package info.tech.dabbaboy.activity.subscription

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.viewpager.widget.ViewPager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.android.material.tabs.TabLayout
import info.tech.dabbaboy.R
import info.tech.dabbaboy.adapter.AdapterChooseMenu
import info.tech.dabbaboy.fragment.MenuFragment
import info.tech.dabbaboy.fragment.ScheduleFragment
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_todays_menu.*
import org.json.JSONException
import org.json.JSONObject

class TodaysMenuActivity : AppCompatActivity(), View.OnClickListener {

    /*constant start*/
    val TYPE_LUNCH = "Lunch";
    val TYPE_DINNER = "Dinner";
    /*constant end*/

    lateinit var ctx: Context
    lateinit var sessionManager: SessionManager

    var type: String? = null
    var pin: String? = null
    var pack_id: String? = null
    var order_id: String? = null

    lateinit var date_list: ArrayList<OrderDates>

    // tab layout
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todays_menu)
        initXml()
        getIntentData()
        if (ConnectionDetector.isConnected()) {
            getOrderDates(ApiObjects.getOrderdates(order_id))
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinator_choosemenu)
        }
    }

    private fun getIntentData() {
        pack_id = intent.getStringExtra("pack_id")
        order_id = intent.getStringExtra("order_id")
    }

    fun getPincode(): String? {
        return pin
    }

    fun getPackid(): String? {
        return pack_id
    }

    private fun getOrderDates(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ORDER_DATES)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setDateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setDateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val user_object = response.getJSONObject("data")
                var date_data = user_object.getJSONArray("user_order")
                if (date_data.length() > 0) {
                    date_list = JsonParser.getOrderDatelistfromToday(date_data)
                    setPagination(getFilterDateList(date_list))
                }
            } else {
                Utility.snacbarShow(message, coordinator_choosemenu)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun getFilterDateList(dateList: ArrayList<OrderDates>): ArrayList<OrderDates> {
        var list: ArrayList<OrderDates> = ArrayList<OrderDates>()

        for (i in 0..dateList.size - 1) {
            var order = dateList[i]
            if (order.lunch_status.equals("1") || order.dinner_status.equals("1")) {
                list.add(order)
            }
        }
        return list
    }

    private fun setPagination(filterDateList: ArrayList<OrderDates>) {
        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)
        if (tabLayout!!.tabCount > 0) {
            tabLayout!!.removeAllTabs()
        }

        for (i in 0..filterDateList.size - 1) {
            tabLayout!!.addTab(
                tabLayout!!.newTab().setText(Utility.changeDateFormat(filterDateList[i].date))
            )
        }
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout!!.tabMode = TabLayout.MODE_SCROLLABLE

        Utility.setMenuFragment(
            MenuFragment(ctx, filterDateList[0], 0),
            "", this@TodaysMenuActivity
        )


/*
        val adapter =
            AdapterChooseMenu(this, supportFragmentManager, tabLayout!!.tabCount, filterDateList)
        viewPager!!.adapter = adapter
        viewPager!!.offscreenPageLimit = 0

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
*/

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
//                viewPager!!.currentItem = tab.position
                Utility.setMenuFragment(
                    MenuFragment(ctx, filterDateList[tab.position], tab.position),
                    "", this@TodaysMenuActivity
                )
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }


    private fun initXml() {
        ctx = this
        sessionManager = SessionManager(ctx)

        iv_header_back.setOnClickListener(this)
        tv_header_title.setText(ctx.getString(R.string.title_choosemenu))
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.iv_header_back -> {
                finish()
            }
        }
    }

    fun setRefresh(position: Int) {
        sessionManager?.setIntData(SessionManager.KEY_MENU_POSISTION, position)
        finish()
        startActivity(intent.putExtra("pos", position))
    }


}
