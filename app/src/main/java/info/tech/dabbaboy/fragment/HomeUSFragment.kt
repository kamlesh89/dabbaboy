package info.tech.dabbaboy.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.subscription.PackagesActivity
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.util.SessionManager
import info.tech.dabbaboy.util.Utility

class HomeUSFragment(var ctx: Context, var title: String) : Fragment(), View.OnClickListener {

    var sessionManager: SessionManager? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_home_us, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View?) {
        myview?.findViewById<Button>(R.id.bt_homeus_subscribe)?.setOnClickListener(this)
        myview?.findViewById<TextView>(R.id.tv_homeus_address)?.setOnClickListener(this)
        myview?.findViewById<TextView>(R.id.bt_homeus_seepackage)?.setOnClickListener(this)
        myview?.findViewById<TextView>(R.id.bt_homeus_callnow)?.setOnClickListener(this)
        myview?.findViewById<TextView>(R.id.bt_homeus_whatsapp)?.setOnClickListener(this)

        myview?.findViewById<TextView>(R.id.tv_homeus_address)
            ?.setText(SessionManager(ctx).getData(SessionManager.KEY_AREA) + ", Indore")

        sessionManager?.setData(SessionManager.KEY_SELECTED_ORDER_ID, "")
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.bt_homeus_subscribe -> {
                startActivity(Intent(ctx, PackagesActivity::class.java))
            }

            R.id.bt_homeus_seepackage -> {
                startActivity(Intent(ctx, PackagesActivity::class.java))
            }

            R.id.tv_homeus_address -> {
                startActivityForResult(
                    Intent(ctx, AddressActivity::class.java),
                    123
                )
            }
            R.id.bt_homeus_whatsapp -> {
            }

            R.id.bt_homeus_callnow -> {
                (ctx as MainActivity).setFrag(
                    ContactUsFragment(ctx, ctx.getString(R.string.title_contactus)),
                    ctx.getString(R.string.title_contactus)
                )
            }
        }
    }
}
