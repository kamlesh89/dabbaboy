package info.tech.dabbaboy.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.subscription.TodaysMenuActivity
import info.tech.dabbaboy.adapter.AdapterVendorMenu
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.pojo.VendorCombo
import info.tech.dabbaboy.util.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MenuFragment(
    var ctx: Context,
    var todayOrder: OrderDates,
    var pos: Int
) : Fragment(), View.OnClickListener {

    var recyclerView: RecyclerView? = null
    var heading_tv: TextView? = null
    var selected_tv: TextView? = null
    var change_bt: Button? = null
    var type_sp: Spinner? = null
    var coordinatorLayout: CoordinatorLayout? = null


    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_menu, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View?) {

        recyclerView = myview?.findViewById(R.id.rv_menu_recyclerview)
        heading_tv = myview?.findViewById(R.id.tv_menu_heading)
        selected_tv = myview?.findViewById(R.id.tv_menu_selecteditem)
        change_bt = myview?.findViewById(R.id.bt_menu_change)
        type_sp = myview?.findViewById(R.id.sp_menu_type)
        coordinatorLayout = myview?.findViewById(R.id.coordinator_menu)
        setSpinner()

        change_bt?.setOnClickListener(this)
    }

    private fun setSpinner() {

        var typelist = gettypeList()
        type_sp?.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, typelist)
        type_sp?.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (isValid()) {
                    if (type_sp?.selectedItem.toString().equals(TYPE_LUNCH)) {
                        var pincode = todayOrder.lunch_full_add?.getString("pin_code")
                        callApi(pincode!!, type_sp?.selectedItem.toString())
                    } else {
                        var pincode = todayOrder.dinner_full_add?.getString("pin_code")
                        callApi(pincode!!, type_sp?.selectedItem.toString())
                    }
                } else {
                    Utility.toastView("Address Not Selected", ctx)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    fun callApi(pincode: String, type: String) {
        if (ConnectionDetector.isConnected()) {
            getVendorcomboes(ApiObjects.getvendorCombo(pincode, type, todayOrder))
        } else {
            Utility.toastView(ctx.getString(R.string.no_internet), ctx)
        }
    }

    private fun isValid(): Boolean {
        if (type_sp?.selectedItem.toString().equals(TYPE_LUNCH)) {
            return !todayOrder.lunch_add_id.equals("0")
        } else {
            return !todayOrder.dinner_add_id.equals("0")
        }
    }

    private fun gettypeList(): ArrayList<String> {
        var list = ArrayList<String>()
        if (todayOrder.lunch_status.equals("1")) {
            list.add(TYPE_LUNCH)
        }
        if (todayOrder.dinner_status.equals("1")) {
            list.add(TYPE_DINNER)
        }
        return list
    }

    private fun getVendorcomboes(apibody: JSONObject) {
        var dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.getWindow()?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.layout_progress)
        dialog.setCancelable(false)
        dialog.show()
        AndroidNetworking.post(WebApis.API_VENDOR_BY_PINCODE)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    dialog.dismiss()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    dialog.dismiss()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val data_obj = response.getJSONObject("data")
                var fix_array = data_obj.getJSONArray("Fix")
                var vendor_array = data_obj.getJSONArray("Vendor")
                var user_menu_array = data_obj.getJSONArray("user_menu")
                var fix_tem = JsonParser.getFixitems(fix_array);
                var list = JsonParser.getvendorCombo(fix_tem, vendor_array)
                if (user_menu_array.length() > 0) {
                    var my_item = JsonParser.getUserMenu(user_menu_array)
                    heading_tv?.setText("My Selected Menu")
                    selected_tv?.setText(fix_tem + ", " + my_item)
                    selected_tv?.visibility = View.VISIBLE
                    change_bt?.visibility = View.VISIBLE
                    recyclerView?.visibility = View.GONE
                    setAdapter(list, "1")
                } else if (vendor_array.length() > 0) {
                    heading_tv?.setText("Choose Menu")
                    recyclerView?.visibility = View.VISIBLE
                    change_bt?.visibility = View.GONE
                    selected_tv?.visibility = View.GONE
                    setAdapter(list, "0")
                } else {
                    heading_tv?.setText("Vendors Not Available at your Selected Address")
                    selected_tv?.visibility = View.GONE
                    recyclerView?.visibility = View.GONE
                    change_bt?.visibility = View.GONE
                }
            } else {
                Utility.toastView(message, ctx)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setAdapter(list: ArrayList<VendorCombo>, mode: String) {
        recyclerView?.setHasFixedSize(true)
        recyclerView?.setLayoutManager(LinearLayoutManager(ctx))
        recyclerView?.adapter = AdapterVendorMenu(
            ctx, list, type_sp?.selectedItem.toString(), todayOrder, pos!!, mode
        )
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.bt_menu_change -> {
                var type = type_sp?.selectedItem.toString()
                if (type.equals(TYPE_LUNCH)) {
                    if (isValidLunch()) {
                        recyclerView?.visibility = View.VISIBLE
                    }
                } else {
                    if (isValidDinner()) {
                        recyclerView?.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun isValidLunch(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (todayOrder.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.lunch_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Change Today Lunch Menu", coordinatorLayout!!)
                return false
            }
        }
        return true
    }

    private fun isValidDinner(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (todayOrder.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.dinner_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Change Today Dinner Menu", coordinatorLayout!!)
                return false
            }
        }
        return true
    }


}
