package info.tech.dabbaboy.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_packagedetails.*
import org.json.JSONException
import org.json.JSONObject

class WalletFragment(var ctx: Context, var title: String) : Fragment() {

    var sessionManager: SessionManager
    var nodata_tv: TextView? = null
    var amount_tv: TextView? = null
    var tiffin_tv: TextView? = null
    var view_ll: LinearLayout? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_wallet, container, false)
        initXml(myview)
        if (ConnectionDetector.isConnected()) {
            getUserProfile(ApiObjects.getUserData(sessionManager?.getData(SessionManager.KEY_ID)))
        }
        return myview
    }

    private fun initXml(myview: View?) {
        nodata_tv = myview?.findViewById(R.id.tv_wallet_notavail)
        amount_tv = myview?.findViewById(R.id.tv_wallet_amount)
        tiffin_tv = myview?.findViewById(R.id.tv_wallet_qty)
        view_ll = myview?.findViewById(R.id.ll_wallet_view)
    }

    private fun getUserProfile(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_USER_PROFILE)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val user_object = response.getJSONObject("data")
                var usertype = user_object.getString("subscribe_status")
                sessionManager?.setData(SessionManager.KEY_USER_SUBSCRIBE, usertype)
                if (usertype.equals("1")) {
                    view_ll?.visibility = View.VISIBLE
                    amount_tv?.setText(sessionManager.getData(SessionManager.KEY_USER_AMOUNT))
                    tiffin_tv?.setText(sessionManager.getData(SessionManager.KEY_USER_TIFFIN))
                } else {
                    nodata_tv?.visibility = View.VISIBLE
                }
            } else {
                nodata_tv?.visibility = View.VISIBLE
                Utility.snacbarShow(message, coordinator_packagedetail)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

}
