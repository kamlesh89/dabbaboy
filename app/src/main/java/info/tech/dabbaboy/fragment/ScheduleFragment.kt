package info.tech.dabbaboy.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.subscription.SchedulingActivity
import info.tech.dabbaboy.adapter.AdapterDialogTiming
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.util.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class ScheduleFragment(
    var ctx: Context,
    var orderDates: OrderDates,
    var position: Int
) : Fragment(),
    View.OnClickListener {

    // lunch
    var lunch_tv: TextView? = null
    var lunch_time_ll: LinearLayout? = null
    var lunch_time_tv: TextView? = null
    var lunch_add_tv: TextView? = null
    var lunch_add_ll: LinearLayout? = null

    // dinner
    var dinner_tv: TextView? = null
    var dinner_time_ll: LinearLayout? = null
    var dinner_time_tv: TextView? = null
    var dinner_add_ll: LinearLayout? = null
    var dinner_add_tv: TextView? = null

    var coordinatorLayout: CoordinatorLayout? = null

    //
    var sessionManager: SessionManager? = null

    //    update time type
    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    val TYPE_LUNCH_ON = "Lunch ON"
    val TYPE_LUNCH_OFF = "Lunch OFF"
    val TYPE_DINNER_ON = "Dinner ON"
    val TYPE_DINNER_OFF = "Dinner OFF"

    //    update address intent
    val LUNCH_ADDRESS = 100
    val DINNER_ADDRESS = 101

    init {
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_schedule, container, false)
        initXml(myview)
        setData()
        return myview
    }

    private fun initXml(myview: View?) {
        lunch_tv = myview?.findViewById(R.id.tv_scheduling_lunch)
        dinner_tv = myview?.findViewById(R.id.tv_scheduling_dinner)
        lunch_time_ll = myview?.findViewById(R.id.ll_schduling_lunchtimeview)
        dinner_time_ll = myview?.findViewById(R.id.ll_schduling_dinnertimeview)
        lunch_add_ll = myview?.findViewById(R.id.ll_schduling_lunchaddressview)
        dinner_add_ll = myview?.findViewById(R.id.ll_schduling_dinneraddressview)
        lunch_time_tv = myview?.findViewById(R.id.tv_schduling_lunchtime)
        dinner_time_tv = myview?.findViewById(R.id.tv_schduling_dinnertime)
        lunch_add_tv = myview?.findViewById(R.id.tv_schduling_addresslunch)
        dinner_add_tv = myview?.findViewById(R.id.tv_schduling_addressdinner)
        coordinatorLayout = myview?.findViewById(R.id.coordinator_sched_fragment)

        myview?.findViewById<ImageView>(R.id.iv_schduling_editlunchtime)?.setOnClickListener(this)
        myview?.findViewById<ImageView>(R.id.iv_schduling_editdinnertime)?.setOnClickListener(this)
        lunch_add_tv?.setOnClickListener(this)
        dinner_add_tv?.setOnClickListener(this)
        lunch_tv?.setOnClickListener(this)
        dinner_tv?.setOnClickListener(this)

    }

    private fun setData() {

        var on_img: Drawable = ctx.resources.getDrawable(R.drawable.switch_on)
        var off_img: Drawable = ctx.resources.getDrawable(R.drawable.switch_off)

        // lunch
        if (orderDates.lunch_status.equals("1")) {

            lunch_time_ll?.visibility = View.VISIBLE
            lunch_add_ll?.visibility = View.VISIBLE

            lunch_tv?.text = TYPE_LUNCH_ON
            lunch_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, on_img, null)
            lunch_time_tv?.setText(orderDates.lunch_time)
            lunch_add_tv?.setText(Utility.getAddress(orderDates.lunch_full_add))
        } else {
            lunch_time_ll?.visibility = View.GONE
            lunch_add_ll?.visibility = View.GONE
            lunch_tv?.text = TYPE_LUNCH_OFF
            lunch_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, off_img, null)
        }

        // dinner
        if (orderDates.dinner_status.equals("1")) {
            dinner_time_ll?.visibility = View.VISIBLE
            dinner_add_ll?.visibility = View.VISIBLE

            dinner_tv?.text = TYPE_DINNER_ON
            dinner_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, on_img, null)
            dinner_time_tv?.setText(orderDates.dinner_time)
            dinner_add_tv?.setText(Utility.getAddress(orderDates.dinner_full_add))
        } else {
            dinner_time_ll?.visibility = View.GONE
            dinner_add_ll?.visibility = View.GONE
            dinner_tv?.text = TYPE_DINNER_OFF
            dinner_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, off_img, null)
        }
    }


    override fun onClick(p0: View?) {

        when (p0?.id) {

            R.id.iv_schduling_editlunchtime -> {
                if (isValidLunch()) {
                    loadTimimg(ApiObjects.getMode("0"), TYPE_LUNCH)
                }
            }
            R.id.iv_schduling_editdinnertime -> {
                if (isValidDinner()) {
                    loadTimimg(ApiObjects.getMode("0"), TYPE_DINNER)
                }
            }
            R.id.tv_schduling_addresslunch -> {
                if (isValidLunch()) {
                    activity?.startActivityForResult(
                        Intent(ctx, AddressActivity::class.java),
                        LUNCH_ADDRESS
                    )
                }
            }
            R.id.tv_schduling_addressdinner -> {
                if (isValidDinner()) {
                    activity?.startActivityForResult(
                        Intent(ctx, AddressActivity::class.java),
                        DINNER_ADDRESS
                    )
                }
            }
            R.id.tv_scheduling_lunch -> {
                if (isValidLunch()) {
                    if (lunch_tv?.text.toString().equals(TYPE_LUNCH_OFF)) {
                        sceduleLunchDinner(
                            ApiObjSchdule.addLunchfromSchedule(
                                orderDates, (ctx as SchedulingActivity).getLastOrder(),
                                sessionManager!!.getData(SessionManager.KEY_ID)
                            )
                        )
                    } else {
                        sceduleLunchDinner(
                            ApiObjSchdule.removeLunchfromSchedule(
                                orderDates, (ctx as SchedulingActivity).getLastOrder(),
                                sessionManager!!.getData(SessionManager.KEY_ID)
                            )
                        )
                    }
                }
            }
            R.id.tv_scheduling_dinner -> {
                if (isValidDinner()) {
                    if (dinner_tv?.text.toString().equals(TYPE_DINNER_OFF)) {
                        sceduleLunchDinner(
                            ApiObjSchdule.addDinnerfromSchedule(
                                orderDates, (ctx as SchedulingActivity).getLastOrder(),
                                sessionManager!!.getData(SessionManager.KEY_ID)
                            )
                        )
                    } else {
                        sceduleLunchDinner(
                            ApiObjSchdule.removeDinnerfromSchedule(
                                orderDates, (ctx as SchedulingActivity).getLastOrder(),
                                sessionManager!!.getData(SessionManager.KEY_ID)
                            )
                        )
                    }
                }
            }
        }
    }

    private fun isValidLunch(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (orderDates.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.lunch_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Edit Today Lunch Order", coordinatorLayout!!)
                return false
            }
        }
        return true
    }

    private fun isValidDinner(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (orderDates.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.dinner_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Edit Today Dinner Order", coordinatorLayout!!)
                return false
            }
        }
        return true
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == LUNCH_ADDRESS && resultCode == Activity.RESULT_OK) {
            var address = data?.getStringExtra("address")
            lunch_add_tv?.setText(address)
            var add_id = data?.getStringExtra("id")
            updateSingleOrder(ApiObjSchdule.updateLunchAddress(orderDates.id, add_id!!))
        } else if (requestCode == DINNER_ADDRESS && resultCode == Activity.RESULT_OK) {
            var address = data?.getStringExtra("address")
            dinner_add_tv?.setText(address)
            var add_id = data?.getStringExtra("id")
            updateSingleOrder(ApiObjSchdule.updateDinnerAddress(orderDates.id, add_id!!))
        }
    }

    // edit time
    private fun loadTimimg(mode: JSONObject, type: String) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_SERVICE_TIME)
            .addJSONObjectBody(mode)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setTimeResponse(response, type)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setTimeResponse(response: JSONObject, type: String) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            if (status == 200) {
                val data_obj = response.getJSONObject("data")
                var lunch_array = data_obj.getJSONArray("Lunch")
                var dinner_array = data_obj.getJSONArray("Dinner")
                var lunch_list = JsonParser.getupdatetimelist(lunch_array)
                var dinner_list = JsonParser.getupdatetimelist(dinner_array)
                if (type == TYPE_LUNCH) {
                    showTimeDialog(lunch_list, type)
                } else {
                    showTimeDialog(dinner_list, type)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun showTimeDialog(
        list: ArrayList<String>,
        type: String
    ) {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_items)
        dialog.getWindow()?.setLayout(
            ((Utility.getWidth(ctx) / 100) * 90),
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        var title_tv = dialog.findViewById<TextView>(R.id.tv_dialogitem_title)
        var cancel_bt = dialog.findViewById<Button>(R.id.bt_dialogaitem_cancel)
        var done_bt = dialog.findViewById<Button>(R.id.bt_dialogaitem_ok)
        var recyclerView = dialog.findViewById<RecyclerView>(R.id.rv_dialogaitem_recycler)

        title_tv.setText("Select One")
        recyclerView?.setHasFixedSize(true)
        recyclerView?.setLayoutManager(LinearLayoutManager(ctx))
        recyclerView?.adapter = AdapterDialogTiming(ctx, list)
        cancel_bt.setOnClickListener(View.OnClickListener { dialog.dismiss() })
        done_bt.setOnClickListener(View.OnClickListener {
            var selected_time =
                sessionManager?.getData(SessionManager.KEY_USER_SELECTED_TIME);
            if (!selected_time.equals("")) {
                dialog.dismiss()
                if (type.equals(TYPE_LUNCH)) {
                    updateSingleOrder(
                        ApiObjSchdule.updateLunchtime(
                            orderDates.id,
                            selected_time!!
                        )
                    )
                } else {
                    updateSingleOrder(
                        ApiObjSchdule.updateDinnertime(
                            orderDates.id,
                            selected_time!!
                        )
                    )
                }
            }
        })
        sessionManager?.setData(SessionManager.KEY_USER_SELECTED_TIME, "")
        dialog.show()
    }


    //update
    private fun updateSingleOrder(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_SINGLE_ORDER)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                (ctx as SchedulingActivity).setRefresh(position)
            } else {
                Utility.toastView(message, ctx)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    // schedule luch and dinner
    private fun sceduleLunchDinner(apibody: JSONObject) {
        if (isValidSchedule()) {
            Utility.showLoader(ctx)
            AndroidNetworking.post(WebApis.API_UPDATE_DAILY_ORDER)
                .addJSONObjectBody(apibody)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Utility.hideLoader()
                        setupdateResponse(response)
                    }

                    override fun onError(anError: ANError) {
                        Utility.hideLoader()
                    }
                })
        } else {
            Utility.toastView("You can not Schedule Last Order", ctx)
        }
    }

    private fun isValidSchedule(): Boolean {
        if (orderDates.id.equals((ctx as SchedulingActivity).getLastOrder().id)) {
            return false
        } else {
            return true
        }
    }
}
