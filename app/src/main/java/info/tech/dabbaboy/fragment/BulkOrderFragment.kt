package info.tech.dabbaboy.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import info.tech.dabbaboy.R
import kotlinx.android.synthetic.main.fragment_bulkorder.*

class BulkOrderFragment(var ctx: Context, var title: String) : Fragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_bulkorder, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View?) {
        myview?.findViewById<TextView>(R.id.tv_bulkorder_date)?.setOnClickListener(this)
        myview?.findViewById<TextView>(R.id.tv_bulkorder_time)?.setOnClickListener(this)
        myview?.findViewById<Button>(R.id.bt_bulkorder_request)?.setOnClickListener(this)
    }

    fun timeDialog() {
        val dialog = Dialog(ctx)
        val window = dialog.window
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
        dialog.setContentView(R.layout.dialog_time)
        val yes_bt =
            dialog.findViewById<Button>(R.id.bt_dialogatime_ok)
        yes_bt.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.tv_bulkorder_time -> {
                timeDialog()
            }
            R.id.tv_bulkorder_date -> {

            }
            R.id.bt_bulkorder_request -> {
                timeDialog()
            }
        }
    }

}
