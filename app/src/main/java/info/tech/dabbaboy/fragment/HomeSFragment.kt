package info.tech.dabbaboy.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.activity.subscription.BuyExtraActivity
import info.tech.dabbaboy.activity.subscription.DeliveryAddressActivity
import info.tech.dabbaboy.activity.subscription.SchedulingActivity
import info.tech.dabbaboy.activity.subscription.TodaysMenuActivity
import info.tech.dabbaboy.pojo.ActivePackage
import info.tech.dabbaboy.util.*
import kotlinx.android.synthetic.main.activity_packagedetails.*
import kotlinx.android.synthetic.main.fragment_home_s.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeSFragment(var ctx: Context, var title: String) : Fragment(), View.OnClickListener {

    var sessionManager: SessionManager? = null
    lateinit var coordinatorLayout: CoordinatorLayout
    var myview: View? = null
    var lunch_add_tv: TextView? = null
    var dinner_add_tv: TextView? = null
    var activepack_sp: Spinner? = null
    var amount_tv: TextView? = null
    var tiffin_tv: TextView? = null
    var edit_date_iv: ImageView? = null
    lateinit var active_packs: ArrayList<ActivePackage>

    var activePackage: ActivePackage? = null
    val INTENT_LUNCH_ADDRESS = 101
    val INTENT_DINNER_ADDRESS = 102

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myview = inflater.inflate(R.layout.fragment_home_s, container, false)
        initXml()
        return myview
    }

    private fun initXml() {

        amount_tv = myview?.findViewById(R.id.tv_homes_amount)
        tiffin_tv = myview?.findViewById(R.id.tv_homes_tiffinqty)
        var scedule_tv = myview?.findViewById<TextView>(R.id.tv_homes_scedule)
        var buyextra_tv = myview?.findViewById<TextView>(R.id.tv_homes_buyextra)
        var menu_tv = myview?.findViewById<TextView>(R.id.tv_homes_choosemenu)

        lunch_add_tv = myview?.findViewById(R.id.tv_homes_lunchaddress)
        dinner_add_tv = myview?.findViewById(R.id.tv_homes_dinneraddress)
        activepack_sp = myview?.findViewById(R.id.sp_homes_activepack)
        edit_date_iv = myview!!.findViewById(R.id.iv_homes_edit)
        coordinatorLayout = myview!!.findViewById(R.id.coordinator_homes)

        menu_tv?.setOnClickListener(this)
        buyextra_tv?.setOnClickListener(this)
        scedule_tv?.setOnClickListener(this)
        lunch_add_tv?.setOnClickListener(this)
        dinner_add_tv?.setOnClickListener(this)
        edit_date_iv?.setOnClickListener(this)

        if (ConnectionDetector.isConnected()) {
            getUserDetails(ApiObjects.getUserdetails(sessionManager!!.getData(SessionManager.KEY_ID)))
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout)
        }
    }

    private fun getUserDetails(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_USER_ADDRESS_DETAILS)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                val data_array = response.getJSONArray("data")
                active_packs = JsonParser.getActivePackage(data_array)
                setActiveSpinner()
            } else {
                Utility.snacbarShow(message, coordinatorLayout)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setActiveSpinner() {
        var list = ArrayList<String>()
        for (i in 0..active_packs!!.size - 1) {
            var no = i + 1
            list.add("Package $no ")
        }
        activepack_sp?.adapter =
            ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_dropdown_item, list)

        activepack_sp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                setData(active_packs[position])
            }
        }
    }

    private fun setData(activePack: ActivePackage) {

        activePackage = activePack
        sessionManager?.setData(SessionManager.KEY_SELECTED_ORDER_ID, activePack.id)
        var lunch_layout = myview?.findViewById<LinearLayout>(R.id.ll_homes_lunchaddress)
        var dinner_layout = myview?.findViewById<LinearLayout>(R.id.ll_homes_dinneraddress)

        var lunch = activePack.orderdetail.getString("lunch")
        var dinner = activePack.orderdetail.getString("dinner")

        var amount = activePack.orderdetail.getString("remaining_total_amount")
        var tiffin = activePack.orderdetail.getString("remaining_quantity")
        sessionManager?.setData(SessionManager.KEY_USER_AMOUNT, amount)
        sessionManager?.setData(SessionManager.KEY_USER_TIFFIN, tiffin)

        amount_tv?.setText(amount)
        tiffin_tv?.setText(tiffin)

        if (lunch.equals("no")) {
            lunch_layout?.visibility = View.GONE
        } else {
            lunch_layout?.visibility = View.GONE
            sessionManager?.setData(
                SessionManager.KEY_USER_LUNCH_ADD,
                Utility.getAddress(activePack.lunch_address)
            )
            lunch_add_tv?.setText(Utility.getAddress(activePack.lunch_address))
        }
        if (dinner.equals("no")) {
            dinner_layout?.visibility = View.GONE
        } else {
            dinner_layout?.visibility = View.GONE
            sessionManager?.setData(
                SessionManager.KEY_USER_DINNER_ADD,
                Utility.getAddress(activePack.dinner_address)
            )
            dinner_add_tv?.setText(Utility.getAddress(activePack.dinner_address))
        }

        var startdate: String = activePack.orderdetail.getString("startdate")
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val today_date: String = df.format(c)

        var start = SimpleDateFormat("yyyy-MM-dd").parse(startdate)
        var today = SimpleDateFormat("yyyy-MM-dd").parse(today_date)

        var total_qty = activePack.orderdetail.getString("quantity")
        var remaining_qty = activePack.orderdetail.getString("remaining_quantity")

        if (!total_qty.equals(remaining_qty)) {
            ll_homes_startdate.visibility = View.GONE
        } else if (start.before(today) || start.equals(today)) {
            ll_homes_startdate.visibility = View.GONE
        } else {
            tv_homes_startdate.text = startdate
            ll_homes_startdate.visibility = View.VISIBLE
        }
    }

    override fun onClick(p0: View?) {

        when (p0?.id) {

            R.id.tv_homes_lunchaddress -> {
                startActivity(Intent(ctx, DeliveryAddressActivity::class.java))
            }

            R.id.iv_homes_edit -> {
                showCalendar()
            }

            R.id.tv_homes_dinneraddress -> {
                startActivity(Intent(ctx, DeliveryAddressActivity::class.java))
            }

            R.id.tv_homes_choosemenu -> {
                var pack_id =
                    active_packs[activepack_sp!!.selectedItemPosition].orderdetail.getString("package_id")
                if (checkPayment()) {
                    startActivity(
                        Intent(ctx, TodaysMenuActivity::class.java)
                            .putExtra(
                                "order_id",
                                active_packs[activepack_sp!!.selectedItemPosition].id
                            )
                            .putExtra("pack_id", pack_id)
                    )
                }
            }

            R.id.tv_homes_scedule -> {
                sessionManager?.setIntData(SessionManager.KEY_SCHEDULE_POSISTION, 0)
                if (checkPayment()) {
                    startActivity(
                        Intent(ctx, SchedulingActivity::class.java)
                            .putExtra("id", active_packs[activepack_sp!!.selectedItemPosition].id)
                            .putExtra("pos", 0)
                    )
                }
            }

            R.id.tv_homes_buyextra -> {
                sessionManager?.setIntData(SessionManager.KEY_EXTRA_POSISTION, 0)
                startActivity(
                    Intent(ctx, BuyExtraActivity::class.java)
                        .putExtra("id", active_packs[activepack_sp!!.selectedItemPosition].id)
                        .putExtra("pos", 0)
                )
            }

        }
    }

    private fun checkPayment(): Boolean {
        var pay_status = activePackage?.orderdetail?.getString("payment_status")
        if (pay_status.equals("0")) {
            Utility.snacbarShow("Please Pay your Order Amount", coordinatorLayout)
            return false
        } else {
            return true
        }
    }

    private fun showCalendar() {
        val date: String = Utility.getDate()
        val mDay = date.substring(8, 10).toInt()
        val mMonth = date.substring(5, 7).toInt() - 1
        val mYear = date.substring(0, 4).toInt()
        val datePickerDialog = DatePickerDialog(
            ctx,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var monthOfYear = monthOfYear
                monthOfYear = monthOfYear + 1
                var day1 = dayOfMonth.toString()
                var month1 = monthOfYear.toString()
                if (day1.length == 1) {
                    day1 = "0$day1"
                }
                if (month1.length == 1) {
                    month1 = "0$month1"
                }
                tv_homes_startdate.setText("$year-$month1-$day1")
                if (ConnectionDetector.isConnected()) {
                    updatestartDate(
                        ApiObjects.updateStartdate(
                            sessionManager!!.getData(SessionManager.KEY_SELECTED_ORDER_ID),
                            tv_homes_startdate.text.toString()
                        )
                    )
                }
            }, mYear, mMonth, mDay
        )
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    //update
    private fun updatestartDate(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_START_DATE)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            Utility.toastView(message, ctx)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

}
