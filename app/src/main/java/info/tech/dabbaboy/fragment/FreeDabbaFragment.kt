package info.tech.dabbaboy.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.activity.subscription.OrderFreeDabbaActivity
import info.tech.dabbaboy.util.*
import org.json.JSONObject

class FreeDabbaFragment(var ctx: Context, var title: String) : Fragment() {

    var not_avil_tv: TextView? = null
    var claim_bt: Button? = null
    var coordinatorLayout: CoordinatorLayout? = null
    var sessionManager: SessionManager? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_freedabba, container, false)
        initXml(myview)
        if (ConnectionDetector.isConnected()) {
            laodData(
                ApiObjects.getUserProfile(sessionManager!!.getData(SessionManager.KEY_ID))
            )
        } else {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
        }

        return myview
    }

    private fun initXml(myview: View?) {
        not_avil_tv = myview?.findViewById(R.id.tv_free_notavailable)
        claim_bt = myview?.findViewById(R.id.bt_free_claim)
        coordinatorLayout = myview?.findViewById(R.id.coordinator)

        claim_bt?.setOnClickListener(View.OnClickListener {
            startActivity(Intent(ctx,OrderFreeDabbaActivity::class.java))
        })
    }

    private fun laodData(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_USE_PROFILE)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            var data_obj = response.getJSONObject("data")
            var user_tiffin = data_obj.getJSONObject("user_order").getString("free_dabba_tiffin")
            if (!user_tiffin.equals("1")) {
                not_avil_tv?.visibility = View.VISIBLE
                claim_bt?.visibility = View.GONE
            }
        } else {
            Utility.snacbarShow(message, coordinatorLayout!!)
        }
    }

}
