package info.tech.dabbaboy.fragment

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.adapter.OrderHistoryAdapter
import info.tech.dabbaboy.pojo.OrderHistory
import info.tech.dabbaboy.util.*
import org.json.JSONObject

class OrderHistoryFragment(var ctx: Context, var title: String) : Fragment(), View.OnClickListener {

    lateinit var recyclerView: RecyclerView
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var header_ll: LinearLayout
    lateinit var packorder_tv: TextView
    lateinit var free_tv: TextView
    var sessionManager: SessionManager

    var order_list: ArrayList<OrderHistory>? = null
    var free_order_list: ArrayList<OrderHistory>? = null
    var order_id: String? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.layout_recyclerview, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View?) {
        recyclerView = myview!!.findViewById(R.id.layout_recyclerview)
        coordinatorLayout = myview!!.findViewById(R.id.coordinator_recycler)
        header_ll = myview!!.findViewById(R.id.ll_layout_tab)
        packorder_tv = myview!!.findViewById(R.id.tv_layout_packorder)
        free_tv = myview!!.findViewById(R.id.tv_layout_freeorder)
        header_ll.visibility = View.VISIBLE
        free_tv.setOnClickListener(this)
        packorder_tv.setOnClickListener(this)

        order_id = sessionManager.getData(SessionManager.KEY_SELECTED_ORDER_ID)
        if (!order_id.equals("")) {
            if (ConnectionDetector.isConnected()) {
                laodData(
                    ApiObjects.getOrderHistor(
                        sessionManager.getData(SessionManager.KEY_ID),
                        order_id!!
                    )
                )
            } else {
                Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout)
            }
        } else {
            recyclerView?.visibility = View.GONE
        }
    }

    private fun laodData(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ORDER_HISTORY)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setResponse(response)
                }

                override fun onError(error: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setResponse(response: JSONObject) {

        var info_obj = response.getJSONObject("info")
        var status = info_obj.getInt("status")
        var message = info_obj.getString("message")
        if (status == 200) {
            recyclerView?.visibility = View.VISIBLE
            var obj_data = response.getJSONObject("data")
            var order_data = obj_data.getJSONArray("user_order")
            var free_order_data = obj_data.getJSONArray("free_tiffin")
            order_list = JsonParser.getOrderHistory(order_data)
            free_order_list = JsonParser.getfreeOrderHistory(free_order_data)
            setAdapter(order_list!!)
        } else {
            recyclerView?.visibility = View.GONE
            Utility.snacbarShow(message, coordinatorLayout)
        }
    }


    private fun setAdapter(list: ArrayList<OrderHistory>) {
        if (list!!.size > 0) {
            recyclerView.visibility = View.VISIBLE
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(ctx)
            recyclerView.adapter = OrderHistoryAdapter(list!!)
        }else{
            recyclerView.visibility = View.GONE
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {

            R.id.tv_layout_packorder -> {
                setAdapter(order_list!!)
                setHighlight(true)
            }
            R.id.tv_layout_freeorder -> {
                setAdapter(free_order_list!!)
                setHighlight(false)
            }
        }
    }

    private fun setHighlight(b: Boolean) {
        if (b) {
            packorder_tv.setTypeface(null, Typeface.BOLD)
            free_tv.setTypeface(null, Typeface.NORMAL)
        } else {
            packorder_tv.setTypeface(null, Typeface.NORMAL)
            free_tv.setTypeface(null, Typeface.BOLD)
        }
    }

}
