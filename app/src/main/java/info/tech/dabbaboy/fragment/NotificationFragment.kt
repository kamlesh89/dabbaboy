package info.tech.dabbaboy.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity
import info.tech.dabbaboy.activity.subscription.OrderFreeDabbaActivity


class NotificationFragment(var ctx: Context, var title: String) : Fragment() {

    var claim_bt: Button? = null

    init {
        (ctx as MainActivity).setHeadTitle(title)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_notification, container, false)
        initXml(myview)
        return myview
    }

    private fun initXml(myview: View?) {
        claim_bt = myview?.findViewById(R.id.bt_free_claim)
        claim_bt?.setOnClickListener(View.OnClickListener {
            startActivity(Intent(ctx, OrderFreeDabbaActivity::class.java))
        })
    }

}
