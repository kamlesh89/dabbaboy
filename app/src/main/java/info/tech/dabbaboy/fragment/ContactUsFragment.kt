package info.tech.dabbaboy.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.MainActivity

class ContactUsFragment(var ctx: Context, var title: String) : Fragment() {

    init {
        (ctx as MainActivity).setHeadTitle(title)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_contactus, container, false)
        return root
    }
}
