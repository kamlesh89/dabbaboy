package info.tech.dabbaboy.fragment

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.subscription.BuyExtraActivity
import info.tech.dabbaboy.activity.subscription.SchedulingActivity
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.util.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class BuyExtraFragment(
    var ctx: Context,
    var orderDates: OrderDates,
    var position: Int
) : Fragment(),
    View.OnClickListener {

    // lunch
    var lunch_tv: TextView? = null
    var lunch_ll: LinearLayout? = null
    var lunch_add_bt: Button? = null
    var lunch_remove_bt: Button? = null
    var lunch_qty_tv: TextView? = null

    // dinner
    var dinner_tv: TextView? = null
    var dinner_ll: LinearLayout? = null
    var dinner_add_bt: Button? = null
    var dinner_remove_bt: Button? = null
    var dinner_qty_tv: TextView? = null

    var coordinatorLayout: CoordinatorLayout? = null

    //
    var sessionManager: SessionManager? = null

    //    update time type
    val TYPE_LUNCH = "Lunch"
    val TYPE_DINNER = "Dinner"

    init {
        sessionManager = SessionManager(ctx)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myview = inflater.inflate(R.layout.fragment_buyextra, container, false)
        initXml(myview)
        setData()
        return myview
    }

    private fun initXml(myview: View?) {
        lunch_tv = myview?.findViewById(R.id.bt_extra_lunch)
        lunch_ll = myview?.findViewById(R.id.ll_extra_lunch)
        lunch_qty_tv = myview?.findViewById(R.id.tv_extra_lunchqty)
        lunch_add_bt = myview?.findViewById(R.id.bt_extra_lunchplus)
        lunch_remove_bt = myview?.findViewById(R.id.bt_extra_lunchremove)

        dinner_tv = myview?.findViewById(R.id.bt_extra_dinner)
        dinner_ll = myview?.findViewById(R.id.ll_extra_dinner)
        dinner_qty_tv = myview?.findViewById(R.id.tv_extra_dinnerqty)
        dinner_add_bt = myview?.findViewById(R.id.bt_extra_dinnerplus)
        dinner_remove_bt = myview?.findViewById(R.id.bt_extra_dinnerremove)
        coordinatorLayout = myview?.findViewById(R.id.coordinator_buyextra)

        lunch_add_bt?.setOnClickListener(this)
        dinner_add_bt?.setOnClickListener(this)
        lunch_remove_bt?.setOnClickListener(this)
        dinner_remove_bt?.setOnClickListener(this)

        if (!isValidAddmore()) {
            lunch_add_bt?.visibility = View.GONE
            dinner_add_bt?.visibility = View.GONE
        }

    }

    private fun setData() {

        var on_img: Drawable = ctx.resources.getDrawable(R.drawable.switch_on)
        var off_img: Drawable = ctx.resources.getDrawable(R.drawable.switch_off)

        // lunch
        if (orderDates.lunch_status.equals("1")) {

            lunch_ll?.visibility = View.VISIBLE
            lunch_qty_tv?.setText(orderDates.lunch_qty)

            lunch_tv?.setText("Lunch ON")
            lunch_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, on_img, null)

            // remove button
            if (orderDates.lunch_qty.equals("1")) {
                lunch_remove_bt?.visibility = View.GONE
            } else {
                lunch_remove_bt?.visibility = View.VISIBLE
            }

        } else {
            lunch_ll?.visibility = View.GONE

            lunch_tv?.setText("Lunch OFF")
            lunch_tv?.setCompoundDrawablesWithIntrinsicBounds(null, null, off_img, null)
        }

        // dinner
        if (orderDates.dinner_status.equals("1")) {
            dinner_ll?.visibility = View.VISIBLE
            dinner_qty_tv?.setText(orderDates.dinner_qty)

            dinner_tv?.setText("Dinner ON")
            dinner_tv?.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, on_img, null)

            // remove button
            if (orderDates.dinner_qty.equals("1")) {
                dinner_remove_bt?.visibility = View.GONE
            } else {
                dinner_remove_bt?.visibility = View.VISIBLE
            }

        } else {
            dinner_ll?.visibility = View.GONE

            dinner_tv?.setText("Dinner OFF")
            dinner_tv?.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, off_img, null)
        }
    }


    override fun onClick(p0: View?) {

        when (p0?.id) {

            R.id.bt_extra_lunchplus -> {
                if (isValidLunch()) {
                    var qty = lunch_qty_tv?.text.toString().toInt()
                    qty = ++qty
                    addExtraTiffin(
                        ApiObjSchdule.addextraLunch(
                            orderDates, (ctx as BuyExtraActivity).getLastOrder(),
                            qty.toString()
                        )
                    )
                }
            }
            R.id.bt_extra_dinnerplus -> {
                if (isValidDinner()) {
                    var qty = dinner_qty_tv?.text.toString().toInt()
                    qty = ++qty
                    addExtraTiffin(
                        ApiObjSchdule.addextraDinner(
                            orderDates, (ctx as BuyExtraActivity).getLastOrder(),
                            qty.toString()
                        )
                    )
                }
            }
            R.id.bt_extra_lunchremove -> {
                if (isValidLunch()) {
                    var qty = lunch_qty_tv?.text.toString().toInt()
                    qty = --qty
                    addExtraTiffin(
                        ApiObjSchdule.removeextraLunch(
                            orderDates, (ctx as BuyExtraActivity).getLastOrder(),
                            qty.toString(), sessionManager!!.getData(SessionManager.KEY_ID)
                        )
                    )
                }
            }
            R.id.bt_extra_dinnerremove -> {
                if (isValidDinner()) {
                    var qty = dinner_qty_tv?.text.toString().toInt()
                    qty = --qty
                    addExtraTiffin(
                        ApiObjSchdule.removeextraDinner(
                            orderDates, (ctx as BuyExtraActivity).getLastOrder(),
                            qty.toString(), sessionManager!!.getData(SessionManager.KEY_ID)
                        )
                    )
                }
            }
        }
    }


    // schedule luch and dinner
    private fun addExtraTiffin(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_ADD_EXTRA)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                (ctx as BuyExtraActivity).setRefresh(position)
            } else {
                Utility.toastView(message, ctx)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun isValidAddmore(): Boolean {
        if (orderDates.id.equals((ctx as BuyExtraActivity).getLastOrder().id)) {
            return false
        } else {
            return true
        }
    }


    private fun isValidLunch(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (orderDates.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.lunch_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Edit Today Lunch Order", coordinatorLayout!!)
                return false
            }
        }
        return true
    }

    private fun isValidDinner(): Boolean {
        var today_date = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)

        if (!ConnectionDetector.isConnected()) {
            Utility.snacbarShow(ctx.getString(R.string.no_internet), coordinatorLayout!!)
            return false
        } else if (orderDates.date.equals(today_date)) {
            val currentTime =
                SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()
            if (currentTime <= Constant.dinner_time_limit) {
                return true
            } else {
                Utility.snacbarShow("Now You Can't Edit Today Dinner Order", coordinatorLayout!!)
                return false
            }
        }
        return true
    }


}
