package info.tech.dabbaboy.pojo

import org.json.JSONArray

data class VendorCombo(
    var fixitm: String,
    var vendor_name: String,
    var vendor_id: String,
    var menu: JSONArray
) {
}