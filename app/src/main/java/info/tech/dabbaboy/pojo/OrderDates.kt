package info.tech.dabbaboy.pojo

import org.json.JSONObject

data class OrderDates(
    var id: String,
    var order_id: String,
    var date: String,
    var lunch_time: String,
    var dinner_time: String,
    var lunch_qty: String,
    var dinner_qty: String,
    var lunch_vendor_id: String,
    var dinner_vendor_id: String,
    var lunch_status: String,
    var dinner_status: String,
    var lunch_add_id: String,
    var dinner_add_id: String,
    var package_name: String,
    var lunch_full_add: JSONObject?,
    var dinner_full_add: JSONObject?,
    var pack_id: String,
    var packing_type: String
) {
}