package info.tech.dabbaboy.pojo

data class OrderHistory(
    val id: String,
    val pack_name: String,
    val date: String,
    val lunch_qty: String,
    val lunch_status: String,
    val lunch_deliv_status: String,
    val dinner_qty: String,
    val dinner_status: String,
    val dinner_deliv_status: String
) {
}