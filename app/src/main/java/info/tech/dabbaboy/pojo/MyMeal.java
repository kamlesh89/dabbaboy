package info.tech.dabbaboy.pojo;

import org.json.JSONArray;

public class MyMeal {

    private String id;
    private String mealname;
    private String image;
    private JSONArray varient_array;
    private JSONArray package_array;

    public MyMeal(String id, String mealname, String image, JSONArray varient_array, JSONArray package_array) {
        this.id = id;
        this.mealname = mealname;
        this.image = image;
        this.varient_array = varient_array;
        this.package_array = package_array;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMealname() {
        return mealname;
    }

    public void setMealname(String mealname) {
        this.mealname = mealname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public JSONArray getVarient_array() {
        return varient_array;
    }

    public void setVarient_array(JSONArray varient_array) {
        this.varient_array = varient_array;
    }

    public JSONArray getPackage_array() {
        return package_array;
    }

    public void setPackage_array(JSONArray package_array) {
        this.package_array = package_array;
    }
}
