package info.tech.dabbaboy.pojo

import android.os.Parcel
import android.os.Parcelable

data class Address(
    var id: String?,
    var type: String?,
    var house_no: String?,
    var street: String?,
    var landmark: String?,
    var pincode: String?,
    var area: String?,
    var city: String?,
    var latitude: Double?,
    var longitude: Double?
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(house_no)
        parcel.writeString(street)
        parcel.writeString(landmark)
        parcel.writeString(pincode)
        parcel.writeString(area)
        parcel.writeString(city)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }

}
