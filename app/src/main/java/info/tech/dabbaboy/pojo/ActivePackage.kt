package info.tech.dabbaboy.pojo

import org.json.JSONObject

data class ActivePackage(
    var id: String,
    var lunch_address_id: String,
    var dinner_address_id: String,
    var lunch_address: JSONObject?,
    var dinner_address: JSONObject?,
    var orderdetail: JSONObject
) {
}