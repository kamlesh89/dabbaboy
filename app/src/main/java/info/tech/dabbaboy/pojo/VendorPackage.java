package info.tech.dabbaboy.pojo;

import android.os.Parcel;
import android.os.Parcelable;


public class VendorPackage implements Parcelable {

    private String package_id;
    private String package_name;
    private String package_image;
    private String service_type;
    private String item_name;
    private String package_var_array;

    public VendorPackage(String package_id, String package_name, String package_image, String service_type, String item_name, String package_var_array) {
        this.package_id = package_id;
        this.package_name = package_name;
        this.package_image = package_image;
        this.service_type = service_type;
        this.item_name = item_name;
        this.package_var_array = package_var_array;
    }

    protected VendorPackage(Parcel in) {
        package_id = in.readString();
        package_name = in.readString();
        package_image = in.readString();
        service_type = in.readString();
        item_name = in.readString();
        package_var_array = in.readString();
    }

    public static final Creator<VendorPackage> CREATOR = new Creator<VendorPackage>() {
        @Override
        public VendorPackage createFromParcel(Parcel in) {
            return new VendorPackage(in);
        }

        @Override
        public VendorPackage[] newArray(int size) {
            return new VendorPackage[size];
        }
    };

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getPackage_image() {
        return package_image;
    }

    public void setPackage_image(String package_image) {
        this.package_image = package_image;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPackage_var_array() {
        return package_var_array;
    }

    public void setPackage_var_array(String package_var_array) {
        this.package_var_array = package_var_array;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(package_id);
        parcel.writeString(package_name);
        parcel.writeString(package_image);
        parcel.writeString(service_type);
        parcel.writeString(item_name);
        parcel.writeString(package_var_array);
    }
}


