package info.tech.dabbaboy.pojo

import org.json.JSONArray

data class HeaderMenu(
    var service_type: String,
    var item_name: String,
    var header_id: String,
    var select_item_id: String,
    var items: JSONArray
) {
}