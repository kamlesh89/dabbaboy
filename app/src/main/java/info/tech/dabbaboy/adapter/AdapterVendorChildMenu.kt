package info.tech.dabbaboy.adapter

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import info.tech.dabbaboy.R
import info.tech.dabbaboy.pojo.HeaderMenu
import info.tech.dabbaboy.pojo.Items
import info.tech.dabbaboy.util.JsonParser
import info.tech.dabbaboy.util.SessionManager
import info.tech.dabbaboy.util.Utility
import kotlin.collections.ArrayList


class AdapterVendorChildMenu(val ctx: Context, var list: List<HeaderMenu>) :
    RecyclerView.Adapter<AdapterVendorChildMenu.MyHolder>() {

    var sessionManager: SessionManager? = null

    init {
        sessionManager = SessionManager(ctx)
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var header_item_tv: TextView? = null
        var select_item_tv: TextView? = null
        var select_item_id_tv: TextView? = null
        var header_ll: LinearLayout? = null

        fun bindItem(headerMenu: HeaderMenu) {
            header_item_tv = itemView.findViewById(R.id.tv_adpchildmenu_header)
            select_item_tv = itemView.findViewById(R.id.tv_adpchildmenu_selectitem)
            select_item_id_tv = itemView.findViewById(R.id.tv_adpchildmenu_selectitemid)
            header_ll = itemView.findViewById(R.id.ll_adpchildmenu_header)
            header_item_tv?.text = headerMenu.item_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var myview: View =
            LayoutInflater.from(ctx).inflate(R.layout.adapter_childmenu, parent, false)
        return MyHolder(myview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        holder.bindItem(list.get(position))
        var item_list = JsonParser.getItemList(list.get(position).items)
        holder.header_ll?.setOnClickListener(View.OnClickListener {
            viewItemDialog(item_list, holder, position)
        })
    }

    private fun viewItemDialog(
        itemList: ArrayList<Items>,
        holder: MyHolder,
        position: Int
    ) {
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_items)
        dialog.getWindow()?.setLayout(
            ((Utility.getWidth(ctx) / 100) * 90),
            LinearLayout.LayoutParams.WRAP_CONTENT
        );

        var title_tv = dialog.findViewById<TextView>(R.id.tv_dialogitem_title)
        var cancel_bt = dialog.findViewById<Button>(R.id.bt_dialogaitem_cancel)
        var done_bt = dialog.findViewById<Button>(R.id.bt_dialogaitem_ok)
        var recyclerView = dialog.findViewById<RecyclerView>(R.id.rv_dialogaitem_recycler)

        title_tv.setText("Select One")
        recyclerView?.setHasFixedSize(true)
        recyclerView?.setLayoutManager(LinearLayoutManager(ctx))
        recyclerView?.adapter = AdapterDialogItem(ctx, itemList)
        cancel_bt.setOnClickListener(View.OnClickListener { dialog.dismiss() })
        done_bt.setOnClickListener(View.OnClickListener {
            var name = sessionManager?.getData(SessionManager.KEY_USER_SELECTED_ITEM)
            var id = sessionManager?.getData(SessionManager.KEY_USER_SELECTED_ITEM_ID)
            sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM, "")
            sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM_ID, "")
            if (!name.isNullOrBlank()) {
                holder.select_item_tv?.text = "( $name )"
                holder.select_item_id_tv?.text = id
                list[position].select_item_id = id!!
                dialog.dismiss()
            }
        })

        if (itemList.size > 0) {
            dialog.show()
        } else {
            holder.select_item_tv?.setTextColor(ctx.getColor(R.color.colorRed))
            holder.select_item_tv?.setText("Not Available")
        }
    }

    fun getIdList(): ArrayList<String> {
        var id_list = ArrayList<String>()
        for (i in 0..list.size - 1) {
            id_list.add(list[i].select_item_id)
        }
        return id_list
    }
}

