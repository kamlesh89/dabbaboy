package info.tech.dabbaboy.adapter

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.util.ApiObjects
import info.tech.dabbaboy.util.MyCalendar
import info.tech.dabbaboy.util.Utility
import info.tech.dabbaboy.util.WebApis
import org.json.JSONException
import org.json.JSONObject


class AdapterCalendar(
    val ctx: Context, var filterlist: List<OrderDates>,
    var lunch_add_id: String, var dinner_add_id: String,
    var alldatelist: List<OrderDates>
) :
    RecyclerView.Adapter<AdapterCalendar.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var type_tv: TextView? = null
        var date_tv: TextView? = null
        var view_ll: LinearLayout? = null

        fun bindItem(orderDates: OrderDates) {

            date_tv = itemView.findViewById(R.id.tv_adpcalendar_date)
            type_tv = itemView.findViewById(R.id.tv_adpcalendar_type)
            view_ll = itemView.findViewById(R.id.ll_adpcalendar_back)
            date_tv?.setText(orderDates.date.substring(8, 10))
            if (getDateStatus(orderDates.lunch_status, orderDates.dinner_status).equals("")) {
                view_ll?.setBackgroundResource(R.color.colorGraylight)
                type_tv?.setText("Off")
            } else {
                type_tv?.setText(getDateStatus(orderDates.lunch_status, orderDates.dinner_status))
            }
        }

        fun getDateStatus(lunchStatus: String, dinnerStatus: String): String {
            var status = "";
            if (lunchStatus.equals("1") && dinnerStatus.equals("1")) {
                status = "L & D"
            } else if (lunchStatus.equals("1") && dinnerStatus.equals("0")) {
                status = "Lunch"
            } else if (lunchStatus.equals("0") && dinnerStatus.equals("1")) {
                status = "Dinner"
            }
            return status;
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var myview: View =
            LayoutInflater.from(ctx).inflate(R.layout.adp_order_date, parent, false)
        return MyHolder(myview)
    }

    override fun getItemCount(): Int {
        return filterlist.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItem(filterlist.get(position))
        holder.itemView.setOnClickListener(View.OnClickListener {
            if (!holder.type_tv?.text.toString().equals("Off")) {
                showupdateDialog(filterlist.get(position), holder.type_tv?.text.toString());
            }
        })
    }

    private fun showupdateDialog(order: OrderDates, type: String) {
        var status = 0;
        val dialog = Dialog(ctx)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_calenderclick)
        dialog.getWindow()?.setLayout(
            ((Utility.getWidth(ctx) / 100) * 90),
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        var title_tv = dialog.findViewById<TextView>(R.id.tv_dialogcal_title)
        var ld_rb = dialog.findViewById<RadioButton>(R.id.rb_dialogcal_lunchdinner)
        var off_rb = dialog.findViewById<RadioButton>(R.id.rb_dialogcal_off)
        var cancel_bt = dialog.findViewById<Button>(R.id.bt_dialogcal_cancel)
        var done_bt = dialog.findViewById<Button>(R.id.bt_dialogcal_ok)

        if (type.equals("Off")) {
            status = 2
            off_rb.isChecked = true
        } else {
            status = 1
            ld_rb.isChecked = true
        }

        cancel_bt.setOnClickListener(View.OnClickListener { dialog.dismiss() })
        done_bt.setOnClickListener(View.OnClickListener {
            var update_status = 0
            if (off_rb.isChecked) update_status = 2
            else update_status = 1
            if (status != update_status) {
                dialog.dismiss()
                updateDailyOrder(
                    ApiObjects.getupdateDailyOrder(
                        order,
                        alldatelist[alldatelist.size - 1],
                        lunch_add_id,
                        dinner_add_id
                    )
                )
            } else {
                dialog.dismiss()
            }
        })
        dialog.show()
    }

    // get meal type
    fun getupdateMeal(lunchStatus: String, dinnerStatus: String): String {
        var status = "";
        if (lunchStatus.equals("1") && dinnerStatus.equals("1")) {
            status = "Lunch & Dinner"
        } else if (lunchStatus.equals("1") && dinnerStatus.equals("0")) {
            status = "Lunch"
        } else if (lunchStatus.equals("0") && dinnerStatus.equals("1")) {
            status = "Dinner"
        }
        return status;
    }

    // update time
    private fun updateDailyOrder(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_UPDATE_DAILY_ORDER)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
            } else {
                Utility.toastView(message, ctx)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

}

