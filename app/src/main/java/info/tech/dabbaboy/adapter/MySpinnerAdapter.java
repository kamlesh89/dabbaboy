package info.tech.dabbaboy.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import info.tech.dabbaboy.R;
import info.tech.dabbaboy.pojo.Varient;


public class MySpinnerAdapter extends ArrayAdapter<Varient> {

    LayoutInflater flater;
    Context ctx;

    public MySpinnerAdapter(Activity context, int resouceId, int textviewId, ArrayList<Varient> list) {

        super(context, resouceId, textviewId, list);
        ctx = context;
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView, position);
    }

    private View rowview(View convertView, int position) {

        Varient rowItem = getItem(position);
        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.adp_spinnerview, null, false);

            holder.txtTitle = rowview.findViewById(R.id.tv_adp_nav);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setTextColor(ContextCompat.getColor(ctx, R.color.colorBlack));
        holder.txtTitle.setText(rowItem.getDays());
        return rowview;
    }

    private class viewHolder {
        TextView txtTitle;
    }
}
