package info.tech.dabbaboy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import info.tech.dabbaboy.R
import info.tech.dabbaboy.pojo.Items
import info.tech.dabbaboy.util.SessionManager


class AdapterDialogItem(
    val ctx: Context,
    var list: List<Items>
) :
    RecyclerView.Adapter<AdapterDialogItem.MyHolder>() {

    open var lastSelectedPosition = -1
    var sessionManager: SessionManager? = null

    init {
        sessionManager = SessionManager(ctx)
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemname_tv: RadioButton? = null

        init {
            itemname_tv = itemView.findViewById(R.id.rb_adpchildchilditem_name)
        }

        fun bindItem(
            items: Items,
            position: Int,
            lastSelectedPosition: Int
        ) {
            itemname_tv?.text = items.name
            if (position == lastSelectedPosition) {
                itemname_tv?.isChecked = true
            } else {
                itemname_tv?.isChecked = false
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var myview: View =
            LayoutInflater.from(ctx).inflate(R.layout.adapter_dialogitem, parent, false)
        return MyHolder(myview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        var item = list.get(position)
        holder.bindItem(item, position, lastSelectedPosition)
        holder.itemname_tv?.setOnClickListener(View.OnClickListener {
            lastSelectedPosition = position
            if (lastSelectedPosition != -1) {
                sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM, item.name)
                sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM_ID, item.id)
            } else {
                sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM, "")
                sessionManager?.setData(SessionManager.KEY_USER_SELECTED_ITEM_ID, "")
            }
            notifyDataSetChanged()
        })
    }

}

