package info.tech.dabbaboy.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import info.tech.dabbaboy.R;
import info.tech.dabbaboy.activity.subscription.PackageDeatilsActivity;
import info.tech.dabbaboy.pojo.VendorPackage;
import info.tech.dabbaboy.util.JsonParser;

public class ExpandPackageAdapter extends RecyclerView.Adapter<ExpandPackageAdapter.HeroViewHolder> {

    private List<VendorPackage> packList;
    private Context context;

    public ExpandPackageAdapter(List<VendorPackage> packList, Context context) {
        this.packList = packList;
        this.context = context;
    }

    @Override
    public HeroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_expand_view, parent, false);
        return new HeroViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HeroViewHolder holder, final int position) {
        VendorPackage pack = packList.get(position);

        int comboname = position + 1;
        holder.combo_tv.setText("Combo : " + comboname);
        holder.details_tv.setText(JsonParser.getCombodetails(packList));
        holder.subscribe_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, PackageDeatilsActivity.class)
                        .putExtra("data", pack)
                        .putExtra("menu", holder.details_tv.getText().toString())
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        if (packList.size() > 0) {
            return 1;
        }
        return packList.size();
    }

    class HeroViewHolder extends RecyclerView.ViewHolder {
        TextView combo_tv, details_tv;
        Button subscribe_bt;
        ImageView imageView;

        HeroViewHolder(View itemView) {
            super(itemView);
            combo_tv = itemView.findViewById(R.id.tv_expandadp_combo);
            details_tv = itemView.findViewById(R.id.tv_expandadp_details);
            subscribe_bt = itemView.findViewById(R.id.bt_expandadp_tap);
        }
    }
}
