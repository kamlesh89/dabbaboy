package info.tech.dabbaboy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.subscription.TodaysMenuActivity
import info.tech.dabbaboy.pojo.OrderDates
import info.tech.dabbaboy.pojo.VendorCombo
import info.tech.dabbaboy.util.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList


class AdapterVendorMenu(
    val ctx: Context,
    var list: List<VendorCombo>,
    var type: String?,
    var todayOrder: OrderDates,
    var pos: Int,
    var mode: String
) :
    RecyclerView.Adapter<AdapterVendorMenu.MyHolder>() {


    var sessionManager: SessionManager? = null

    init {
        sessionManager = SessionManager(ctx)
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var combo_name: TextView? = null
        var fix_item: TextView? = null
        var select_bt: Button? = null
        var recyclerView: RecyclerView? = null

        init {
            combo_name = itemView.findViewById(R.id.tv_adpmenu_comboname)
            fix_item = itemView.findViewById(R.id.tv_adpmenu_fixitem)
            select_bt = itemView.findViewById(R.id.bt_adpmenu_select)
            recyclerView = itemView.findViewById(R.id.rv_adpmenu_recyclerview)
        }

        fun bindItem(vendorCombo: VendorCombo, pos: Int) {
            fix_item?.text = vendorCombo.fixitm
            combo_name?.text = "Combo : " + pos
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var myview: View =
            LayoutInflater.from(ctx).inflate(R.layout.adapter_menu, parent, false)
        return MyHolder(myview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        var header_list = JsonParser.getHeaderMenu(list[position].menu)
        holder.bindItem(list.get(position), position + 1)
        holder.recyclerView?.setHasFixedSize(true)
        holder.recyclerView?.setLayoutManager(LinearLayoutManager(ctx))
        var adapter = AdapterVendorChildMenu(ctx, header_list)
        holder.recyclerView?.adapter = adapter

        holder.select_bt?.setOnClickListener(View.OnClickListener {

            var idlist = adapter.getIdList()
            if (allSelected(idlist)) {
                var userid = sessionManager?.getData(SessionManager.KEY_ID)
                var vendor_id = list[position].vendor_id
                var apiObj =
                    ApiObjects.getSelectCombo(
                        userid,
                        vendor_id,
                        todayOrder.order_id,
                        type,
                        todayOrder.date,
                        todayOrder.pack_id,
                        idlist, mode
                    )
                if (ConnectionDetector.isConnected()) {
                    submitMenuandVendor(apiObj)
                }
            } else {
                Utility.toastView("Please Select All Item", ctx)
            }
        })
    }

    private fun allSelected(idlist: ArrayList<String>): Boolean {
        for (i in 0..idlist.size - 1) {
            if (idlist[i].isNullOrBlank()) {
                return false
            }
        }
        return true
    }

    // submit menu and vendor
    private fun submitMenuandVendor(apibody: JSONObject) {
        Utility.showLoader(ctx)
        AndroidNetworking.post(WebApis.API_SUBMIT_MENU)
            .addJSONObjectBody(apibody)
            .setTag("test")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    Utility.hideLoader()
                    setupdateResponse(response)
                }

                override fun onError(anError: ANError) {
                    Utility.hideLoader()
                }
            })
    }

    private fun setupdateResponse(response: JSONObject) {
        try {
            val info_obj = response.getJSONObject("info")
            val status = info_obj.getInt("status")
            val message = info_obj.getString("message")
            if (status == 200) {
                (ctx as TodaysMenuActivity).setRefresh(pos)
            } else {
                Utility.toastView(message, ctx)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }


}

