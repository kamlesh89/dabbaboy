package info.tech.dabbaboy.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import info.tech.dabbaboy.pojo.OrderDates

class AdapterChooseMenu(
    private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int,
    var datelist: ArrayList<OrderDates>
) :
    FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
//        return MenuFragment(myContext, datelist[position], position)
        return Fragment()
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}