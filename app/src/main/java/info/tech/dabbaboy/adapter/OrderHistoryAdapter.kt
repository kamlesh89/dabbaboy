package info.tech.dabbaboy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import info.tech.dabbaboy.R
import info.tech.dabbaboy.pojo.OrderHistory

class OrderHistoryAdapter(var list: ArrayList<OrderHistory>) :
    RecyclerView.Adapter<OrderHistoryAdapter.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var packname_tv: TextView? = null
        var date_tv: TextView? = null
        var lunch_ll: LinearLayout? = null
        var lunch_qty_tv: TextView? = null
        var lunch_status_tv: TextView? = null
        var dinner_ll: LinearLayout? = null
        var dinner_qty_tv: TextView? = null
        var dinner_status_tv: TextView? = null

        init {
            packname_tv = itemView.findViewById(R.id.tv_adphistory_packname)
            date_tv = itemView.findViewById(R.id.tv_adphistory_date)
            lunch_ll = itemView.findViewById(R.id.ll_adphistory_lunch)
            lunch_qty_tv = itemView.findViewById(R.id.tv_adphistory_lunchqty)
            lunch_status_tv = itemView.findViewById(R.id.tv_adphistory_lunchstatus)
            dinner_ll = itemView.findViewById(R.id.ll_adphistory_dinner)
            dinner_qty_tv = itemView.findViewById(R.id.tv_adphistory_dinnerqty)
            dinner_status_tv = itemView.findViewById(R.id.tv_adphistory_dinnerstatus)
        }

        fun bindView(holder: MyHolder, order: OrderHistory) {
            packname_tv?.setText(order.pack_name)
            date_tv?.setText(order.date)
            if (order.lunch_status.equals("0")) {
                lunch_ll?.visibility = View.GONE
            } else {
                lunch_ll?.visibility = View.VISIBLE
                lunch_qty_tv?.setText("QTY - " + order.lunch_qty)
                lunch_status_tv?.setText("Status - " + getStatusMessage(order.lunch_deliv_status))

            }
            if (order.dinner_status.equals("0")) {
                dinner_ll?.visibility = View.GONE
            } else {
                dinner_ll?.visibility = View.VISIBLE
                dinner_qty_tv?.setText("QTY - " + order.dinner_qty)
                dinner_status_tv?.setText("Status - " + getStatusMessage(order.dinner_deliv_status))
            }
        }

        fun getStatusMessage(status: String): String {
            if (status.equals("0")) {
                return "Not Delivered"
            } else if (status.equals("1")) {
                return "Packed for Delivery"
            } else if (status.equals("2")) {
                return "Allote to Delivery boy"
            } else if (status.equals("3")) {
                return "Pick up by Delivery Boy For delivery"
            } else if (status.equals("4") || status.equals("5") || status.equals("6")) {
                return "Delivered"
            } else {
                return "Not Available"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.adp_order_history, parent, false)
        return MyHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindView(holder, list[position])
    }
}