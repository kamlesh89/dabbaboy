package info.tech.dabbaboy.adapter

import android.Manifest
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import info.tech.dabbaboy.R
import info.tech.dabbaboy.activity.AddAddressActivity
import info.tech.dabbaboy.activity.AddressActivity
import info.tech.dabbaboy.activity.UpdateAddressActivity
import info.tech.dabbaboy.pojo.Address
import info.tech.dabbaboy.util.Constant
import info.tech.dabbaboy.util.Utility


class AdapterAddress(val ctx: Context, var list: List<Address>) :
    RecyclerView.Adapter<AdapterAddress.MyHolder>() {

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var select_bt: Button? = null
        var edit_iv: ImageView? = null

        fun bindItem(address: Address) {

            val type_tv = itemView.findViewById<TextView>(R.id.tv_adpaddress_type)
            val address_tv = itemView.findViewById<TextView>(R.id.tv_adpaddress_address)
            val pin_tv = itemView.findViewById<TextView>(R.id.tv_adpaddress_pin)
            select_bt = itemView.findViewById(R.id.bt_adpaddress_select)
            edit_iv = itemView.findViewById(R.id.iv_adpaddress_edit)

            type_tv.setText(address.type)
            var addr =
                address.house_no + " " + address.street + " " +
                        address.landmark + ", " + address.city
            address_tv.setText(addr)
            pin_tv.setText(address.pincode)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        var myview: View =
            LayoutInflater.from(ctx).inflate(R.layout.adp_address_view, parent, false)
        return MyHolder(myview)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindItem(list.get(position))
        holder.select_bt?.setOnClickListener(View.OnClickListener { view ->
            (ctx as AddressActivity).returnselectedAddress(list[position])
        })
        holder.edit_iv?.setOnClickListener(View.OnClickListener {
            requestPermission(list[position])
        })
    }

    // location permission
    fun requestPermission(address: Address) {
        Dexter.withActivity(ctx as AddressActivity).withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        ctx.startActivityForResult(
                            Intent(ctx, UpdateAddressActivity::class.java)
                                .putExtra("data", address),
                            Constant.ADDRESS_INTENT_CODE
                        )
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Utility.toastView("Error occurred! ", ctx)
            }
            .onSameThread()
            .check()
    }

}

