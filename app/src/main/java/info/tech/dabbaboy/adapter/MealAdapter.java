package info.tech.dabbaboy.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONObject;

import java.util.List;

import info.tech.dabbaboy.R;
import info.tech.dabbaboy.pojo.MyMeal;
import info.tech.dabbaboy.util.Constant;
import info.tech.dabbaboy.util.JsonParser;
import info.tech.dabbaboy.util.WebApis;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.HeroViewHolder> {


    private List<MyMeal> myMealList;
    private Context context;

    private static int currentPosition = -1;

    public MealAdapter(List<MyMeal> myMealList, Context context) {
        this.myMealList = myMealList;
        this.context = context;
    }

    @Override
    public HeroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adp_mymeal, parent, false);
        return new HeroViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HeroViewHolder holder, final int position) {

        MyMeal myMeal = myMealList.get(position);
        holder.name_tv.setText(myMeal.getMealname());
        Glide.with(context)
                .load(myMeal.getImage().replace(" ", "%20"))
                .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(10)))
                .into(holder.imageView);
        holder.recyclerView.setHasFixedSize(true);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        ExpandPackageAdapter adapter = new ExpandPackageAdapter(JsonParser.getvendorPackagelist(myMeal), context);
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.setVisibility(View.GONE);

        if (currentPosition == position) {
            Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.expandable_anim);
            holder.recyclerView.setVisibility(View.VISIBLE);
            holder.recyclerView.startAnimation(slideDown);
            holder.more_bt.setText("Less");
        } else {
            holder.more_bt.setText("More");
        }

        holder.more_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (currentPosition == position) {
                    currentPosition = -1;
                } else {
                    //getting the position of the item to expand it
                    currentPosition = position;
                }
                //reloding the list
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return myMealList.size();
    }

    class HeroViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv;
        Button more_bt;
        ImageView imageView;
        RecyclerView recyclerView;

        HeroViewHolder(View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.tv_adpmeal_mealname);
            more_bt = itemView.findViewById(R.id.bt_adpmeal_more);
            imageView = itemView.findViewById(R.id.iv_adpmeal_image);
            recyclerView = itemView.findViewById(R.id.rv_expand_recyclerview);
        }
    }
}
